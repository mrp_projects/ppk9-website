<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>


<div class="maindiv">

<h1>Protect Police K-9 Photo Gallery</h1>

<div class="photo_gallery_box">
<h3>January 24, 2014 &mdash; Yuma Police vesting event</h3>
<a href="/photos/2014-01-24-Yuma/originals/MJB_0054.jpg"><img src="/photos/2014-01-24-Yuma/150/MJB_0054_150.jpg"></a>
<a href="/photos/2014-01-24-Yuma/originals/MJB_0043.jpg"><img src="/photos/2014-01-24-Yuma/150/MJB_0043_150.jpg"></a>
</div>

<div class="photo_gallery_box">
<h3>May 5, 2012 &mdash; 4-H Event</h3>
<a href="/photos/2012-05-05-4H/600/1.jpg"><img src="/photos/2012-05-05-4H/150/1.jpg"></a>
<!-- <a href="/photos/2012-05-05-4H/600/2.jpg"><img src="/photos/2012-05-05-4H/150/2.jpg"></a> -->
<!-- <a href="/photos/2012-05-05-4H/600/3.jpg"><img src="/photos/2012-05-05-4H/150/3.jpg"></a> -->
<a href="/photos/2012-05-05-4H/600/4.jpg"><img src="/photos/2012-05-05-4H/150/4.jpg"></a>
<a href="/photos/2012-05-05-4H/600/5.jpg"><img src="/photos/2012-05-05-4H/150/5.jpg"></a>
<a href="/photos/2012-05-05-4H/600/6.jpg"><img src="/photos/2012-05-05-4H/150/6.jpg"></a>
<a href="/photos/2012-05-05-4H/600/7.jpg"><img src="/photos/2012-05-05-4H/150/7.jpg"></a>
<a href="/photos/2012-05-05-4H/600/8.jpg"><img src="/photos/2012-05-05-4H/150/8.jpg"></a>
<a href="/photos/2012-05-05-4H/600/9.jpg"><img src="/photos/2012-05-05-4H/150/9.jpg"></a>
<!-- <a href="/photos/2012-05-05-4H/600/10.jpg"><img src="/photos/2012-05-05-4H/150/10.jpg"></a> -->
<a href="/photos/2012-05-05-4H/600/11.jpg"><img src="/photos/2012-05-05-4H/150/11.jpg"></a>
</div>

<div class="photo_gallery_box">
<h3>January 29, 2012 &mdash; Vesting Ceremony</h3>
<a href="/photos/2012-01-29-vesting_ceremony/600/1.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/1.jpg"></a>
<a href="/photos/2012-01-29-vesting_ceremony/600/2.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/2.jpg"></a>
<!-- <a href="/photos/2012-01-29-vesting_ceremony/600/3.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/3.jpg"></a> -->
<a href="/photos/2012-01-29-vesting_ceremony/600/4.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/4.jpg"></a>
<a href="/photos/2012-01-29-vesting_ceremony/600/5.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/5.jpg"></a>
<a href="/photos/2012-01-29-vesting_ceremony/600/6.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/6.jpg"></a>
<a href="/photos/2012-01-29-vesting_ceremony/600/7.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/7.jpg"></a>
<a href="/photos/2012-01-29-vesting_ceremony/600/8.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/8.jpg"></a>
<a href="/photos/2012-01-29-vesting_ceremony/600/9.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/9.jpg"></a>
<a href="/photos/2012-01-29-vesting_ceremony/600/10.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/10.jpg"></a>
<!--
<a href="/photos/2012-01-29-vesting_ceremony/600/11.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/11.jpg"></a>
<a href="/photos/2012-01-29-vesting_ceremony/600/12.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/12.jpg"></a>
-->
<a href="/photos/2012-01-29-vesting_ceremony/600/13.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/13.jpg"></a>
<!-- <a href="/photos/2012-01-29-vesting_ceremony/600/14.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/14.jpg"></a> -->
<a href="/photos/2012-01-29-vesting_ceremony/600/15.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/15.jpg"></a>
<a href="/photos/2012-01-29-vesting_ceremony/600/16.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/16.jpg"></a>
<a href="/photos/2012-01-29-vesting_ceremony/600/17.jpg"><img src="/photos/2012-01-29-vesting_ceremony/150/17.jpg"></a>
</div>

</div>

<?php

include 'includes/footer.php' ;

?>