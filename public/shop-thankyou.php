<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>


<div class="maindiv">

<h1>Thank You for Your Support</h1>

<p>
Your PayPal transation is complete, and you will receive payment confirmation via e-mail within the next few moments.  Thank you for supporting Protect Police K-9.
</p>

</div>

<?php

include 'includes/footer.php' ;

?>