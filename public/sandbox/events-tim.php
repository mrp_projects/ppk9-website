<?

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

$today = date("Ymd") ;                          
$modified = filemtime("events.php") ;
$date = date("F j<\s\u\p>S</\s\u\p>, Y", $modified) ;

?>


<div class="maindiv">

<h1>Upcoming Protect Police K-9 Events</h1>
<ul>
  <li>
    <p><strong>WalMart National Safety Month </strong> - October 2006 </p>
    <p>October is National Safety Month for WalMart; PPK9 will hold fundrasier events each weekend of the month at local WalMart locations. Police K-9 officers will be present to give demonstrations, and PPK9 representatives will be available to answer questions and facilitate your support for our programs. </p>
  <li>
    <p><strong>6th Annual Arizona Animal Fair </strong> - February 17th, 2007 </p>
    <p>Please join us on Saturday, February 17th from 10am to 5pm as Protect Police K9 returns to participate in the Sixth Annual Arizona Animal Fair at <a href="http://maps.yahoo.com/maps_result?ed=y.r5aep_0Tr.EXoNK5i52k1cgNhpKq_QZRrBEfw.tub7&csz=tucson%2C+az&country=us&new=1&name=&qty=" target="_blank">Reid Park </a> in Tucson. </p>
    <p>The Arizona Animal Fair (AAF) is an annual festival attended by over 8000 people (and their pets). The fair features six arenas of non-stop performances (including frisbee, flyball, agility, weigh-pulling, and doggie-dancing) and demonstrations by Police K-9 units and search-and-rescue dogs. More than 100 exhibitor booths provide the opportunity to shop around, try the latest pet products, and learn from industry experts. There will be lots of great animals available for adoption as well! </p>
    <p>This important community event benefits the entire Tucson community, not only by fostering collaboration among local animal rescue and welfare agencies, but also by educating pet owners about adoption, microchipping, and other emerging developments in animal care. All proceeds are returned to the animal rescue community. </p>
    <img src="docs/arizonaanimalfair.gif" width="200" height="127">
    <p>For more information, visit <a href="http://www.safeanimals.com/aaf.html" target="_blank">www.safeanimals.com/aaf.html </a>. </p>
  </li>
  <li>
    <p><strong>5th Annual Desert Dog Police K-9 Trials - April 14th &amp; 15th, 2007</strong></p>
    <p align="justify"> The Arizona Law Enforcement Canine Association (<a href="http://www.alecapolicek9.com" target="_blank">A.L.E.C.A.</a>)  in partnership with the Scottsdale, Mesa &amp; Phoenix Police Departments and Scottsdale Healthcare, would like to invite you to attend the <a href="http://www.desertdogk9trials.com" target="_blank">DESERT DOG POLICE K-9 TRIALS</a>, that will be held at the newly renovated Scottsdale Stadium, on April 14th &amp; 15th, 2007. For more information, please visit <a href="http://www.desertdogk9trials.com" target="_blank">www.desertdogk9trials.com</a>. </p>
  </li>
  </ul>
<p><?
echo 'This listing was last updated on <b>' ;
echo $date ;
echo '</b>.'
?></p>

</div>
<?

include 'includes/footer.php' ;

?>