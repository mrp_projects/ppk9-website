<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>

<div class="maindiv">

<h1>K9 Vest Request Process</h1>

<p>Qualifying canines/handlers must</p>
<ul>
<li>be affiliated with an accredited law enfocement agency in the State of Arizona</li>
<li>make provisions within their department's budget to secure a replacement vest in 5 years</li>
<li>be willing and available to participate in all fundraising efforts related to procuring their vest donation</li>
</ul>

<p>If you meet the program requirements, you may request a vest for your K9 by downloading, printing, and submitting the request/measurements form:</p>
<ul><li><a href="docs/PPK9-VestRequestForm.pdf">PPK9-VestRequestForm.pdf</a></li></ul>
<p>Note: <i>You also will also be required to submit three unit identification arm-patches.</i></p>

</div>

<?php

include 'includes/footer.php' ;

?>

<!--
<ul style="list-style-image:url(images/numbers/1.gif);"><li><p>test 1</p></li></ul>
<ul style="list-style-image:url(images/numbers/2.gif);"><li><p>test 2</p></li></ul>
<ul style="list-style-image:url(images/numbers/3.gif);"><li><p>test 3</p></li></ul>
-->
