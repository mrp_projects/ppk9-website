<?php
include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;
?>

<div class="maindiv">
<h1>Dogs vested by Protect Police K-9</h1>

<p>
Protect Police K-9 is proud to announce that, due to the efforts of our dedicated <a href="directors.php">Board of Directors</a>, our corporate <a href="supporters.php">sponsors</a>, and everyone who has supported our mission by donating time and money to help vest Arizona's police dogs, EVERY K9 in Southern Arizona is protected on duty by ballistic/stab vests.  We are immensely proud of this accomplishment, and deeply appreciate your support.  There are still unprotected dogs in the districts of Northern Arizona who need our help.  Our list of dogs awaiting vest protection is long, but we have confidence that, with your help, we can reach our goal.  To find out how you can help vest Arizona's police dogs, visit our <a href="help.php">How to Help</a> page.
</p>

<p>
<table cellpadding="3" cellspacing="0">
	<tr><td><b>Department<b></td><td align="center"><b>K-9s Vested</b></td></tr>
	<tr><td>Tucson Police Department</td><td align="center">11</td></tr>
	<tr><td>Pima County Sheriff's Department</td><td align="center">7</td></tr>
	<tr><td>University of Arizona Police Department</td><td align="center">5</td></tr>
	<tr><td>Pima Police Department</td><td align="center">1</td></tr>
	<tr><td>Safford Arizona Police Department</td><td align="center">1</td></tr>
	<tr><td>Santa Cruz County Sheriff's Department</td><td align="center">2</td></tr>
	<tr><td>Nogales Police Department</td><td align="center">3</td></tr>
	<tr><td>Union Pacific Rail Road Police Department</td><td align="center">2</td></tr>
	<tr><td>Benson Arizona Police Department</td><td align="center">1</td></tr>
	<tr><td>Graham County Sheriff's Department </td><td align="center">1</td></tr>
	<tr><td>Tohono O'Odham Nation Police Department</td><td align="center">3</td></tr>
	<tr><td>Pasqua Yaqui Tribe Police Department, Arizona</td><td align="center">1</td></tr>
	<tr><td>Sahuarita Police Department </td><td align="center">1</td></tr>
	<tr><td>Arizona Department of Corrections</td><td align="center">25</td></tr>
	<tr><td>Oro Valley Police Department</td><td align="center">3</td></tr>
	<tr><td>Apache Police Whiteriver AZ</td><td align="center">3</td></tr>
	<tr><td>The United States Air Force</td><td align="center">2</td></tr>
	<tr><td>Mesa Police Department</td><td align="center">4</td></tr>
	<tr><td>Arizona Department of Public Safety (DPS)</td><td align="center">27</td></tr>
	<tr><td>Yavapai-Apache Police</td><td align="center">1</td></tr>
	<tr><td>Prescott Valley Police</td><td align="center">1</td></tr>
	<tr><td>Paradise Valley Police</td><td align="center">1</td></tr>
	<tr><td>Scottsdale Police</td><td align="center">2</td></tr>
	<tr><td>El Mirage Police Department</td><td align="center">1</td></tr>
	<tr><td>Maricopa County Sheriff's Department</td><td align="center">12</td></tr>
	<tr><td>Salt River Police</td><td align="center">2</td></tr>
	<tr><td>Tolleson Police</td><td align="center">1</td></tr>
	<tr><td>Avondale Police</td><td align="center">1</td></tr>
	<tr><td>Buckeye Police</td><td align="center">1</td></tr>
	<tr><td>Mohave County Sheriff's Department</td><td align="center">1</td></tr>
	<tr><td>San Carlos Police Department</td><td align="center">1</td></tr>
	<tr><td>Camp Verde Town Marshal's Office</td><td align="center">1</td></tr>
	<tr><td>Abernathy Police Department (Abernathy, Texas)</td><td align="center">1</td></tr>
	<tr><td>Alliance Police Department (Alliance, Ohio)<br /><i>(in memory of Army Special Forces Seargent Pedro "Papi" Munoz)</i></td><td align="center">1</td></tr>
	<tr><td>Globe Police Department</td><td align="center">1</td></tr>
	<tr><td>Casa Grande Police Department</td><td align="center">1</td></tr>
	<tr><td>Florence Police dept Florence AZ</td><td align="center">2</td></tr>
	<tr><td>University Physicians Healthcare (Kino Campus) </td><td align="center">6</td></tr>
	<tr><td>South Tucson Police Department </td><td align="center">1</td></tr>
	<tr><td>Yuma Police Department </td><td align="center">1</td></tr>
	<tr><td>Cochise County Sheriff Office<br><i>(in memory of "Meena")</i></td><td align="center">1</td></tr>
</table>
</p>

<p>
Our success in vesting these K9s is due largely to the contributions of our <a href="supporters.php">corporate sponsors</a>.
</p>

</div>

<?php
include 'includes/footer.php' ;
?>
