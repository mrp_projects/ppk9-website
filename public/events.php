<?

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

$today = date("Ymd") ;                          
$modified = filemtime("events.php") ;
$date = date("F j<\s\u\p>S</\s\u\p>, Y", $modified) ;

?>


<div class="maindiv">

<h1>Upcoming Protect Police K-9 Events</h1>

<ul>

<?php if($today <= 20050219) print '
<li>
<p><b>Arizona Animal Fair</b> - February 18, 2005</p>
<p>Protect Police K9 returns this year to participate again in the Arizona Animal Fair sponsored by <b>S.A.F.E.</b>.  Please join at <a href="http://maps.yahoo.com/maps_result?ed=y.r5aep_0Tr.EXoNK5i52k1cgNhpKq_QZRrBEfw.tub7&csz=tucson%2C+az&country=us&new=1&name=&qty=" target="PPK9_external">Reid Park</a> in Tucson from 10:00 AM to 4:00 PM for live performances, demonstrations, vendor exhibits, food, and fun. Events include performances by the <a href="http://www.skyydogsusa.com/dreamteam.html" target="PPK9_external">Skyy Dogs USA Dream Team K-9s</a> and the <a href="http://www.frisbeek9.com/" target="PPK9_external">Muttley Crew</a>, animal painting, clinics, and more.</p>
<p>Additionally, we are seeking K9 units and handlers to participate in demonstrations.  Please contact <a href="mailto:gil@protectpolicek-9.com">Gil Valdez</a> if you are able to help out.</p>
<img src="images/events/arizonaanimalfair.gif" alt="Arizona Animal Fair logo" />
<p>For more information, visit <a href="http://www.safeanimals.com/aaf.html">www.safeanimals.com/aaf.html</a>.</p>
</li>
' ; ?>

<?php if($today <= 20050417) print '
<li>
<p><b>2005 Desert Dog Police K-9 Trials</b> - April 16-17, 2005</p>
<p>The <b>Arizona Law Enforcement Canine Association</b> (A.L.E.C.A.) invites you to attend the Desert Dog III Police K-9 Trails, April 16<sup>th</sup> and 17<sup>th</sup> at the Scottsdale Municipal Stadium in Scottsdale, Arizona.  The event features police K-9 teams from around the Southwest region competing in area/building searches, an obstacle course, tactical obedience, handler protection scenarios (bite work), and narcotic/explosive detection.  Admission is free.</p>
<img src="images/events/desertdogtrials.gif" alt="ALCEA Desert Dog Trials logo" />
<p>For more information, visit <a href="http://www.desertdogk9trials.com" target="PPK9_external">www.desertdogk9trials.com</a>.</p>
</li>
' ; ?>

<?php if($today <= 20050918) print '
<li>
<p><b>NODTA 1<sup>st</sup> Annual <i>Bad Ass Dog</i> Competition</b> - September 17-18, 2005</p>
<p>The nationally recognized K9 training staff of the Northeast Ohio Dog Training Academy will host the first annual <i><b>B.A.D.</b></i> competition September 17<sup>th</sup> and 18<sup>th</sup> at <a href="http://www.afrc.af.mil/910aw/Directions.htm" target="PPK9_external">Youngstown Air Reserve Station</a> in Vienna, Ohio.  Competitions in obedience, officer protection, tracking, narcotics detection, area search, article search, and building search are open to law enforcement officers and K9 teams, and guests and family members are encouraged to attend.  Trophies will be awarded in each area of competition. Following the contests, NODTA will host a cookout & bonfire for participants and their guests.</p>
<img src="images/events/nodta.gif" alt="Northeast Ohio Dog Training Academy logo" />
<p>Interested parties should also consider donating to the <a href="http://www.nodta.com/Seminar%20Schedule.html" target="PPK9_external">Pedro "Papi" Munoz Memorial Fund</a>.
<p>For more information, visit <a href="http://www.nodta.com" target="PPK9_external">http://www.nodta.com</a>.</p>
</li>
' ; ?>

<?php if($today <= 20051111) print '
<li>
<p><b>NPCA 2005 National Training Seminar and Competition</b> - November 7-11, 2005</p>
<p>The <a href="http://www.npca.net/" target="PPK9_external">National Police Canine Association</a> invites you to participate in the 2005 NPCA National Training Seminar and Competition - the Tucson autumn provides the perfect weather and beautiful surroundings for K9 training.  A variety of training clinics will be offered, as well as opportunities to compete with fellow NPCA members.  NPCA has contracted with the <a href="http://www.ichotelsgroup.com/h/d/hi/hd/tusap" target="PPK9_external">Holiday Inn Palo Verde</a> to host the 2005 nationals and provide discounted room rates for participants and their guests.</p>
<p>For more information about the 2005 Nationals, visit the NPCA website at <a href="http://www.npca.net/" target="PPK9_external">www.npca.net</a>.</p>
<p>To find out more about discounted room rates available to NPCA participants, contact the hotel by phone (520.746.1161) or email (<a href="mailto:holidayinntucson@jqh.com">holidayinntucson@jqh.com</a>).
</li>
' ; ?>

<?php if($today <= 20051113) print '
<li>
<p><b>Tucson Area Police K-9 Trials</b> - November 11-13, 2005</p>
<p>The <b>Arizona Law Enforcement Canine Association</b> (A.L.E.C.A.) invites you to attend the Sixteenth Annual Tucson K-9 Trails in Tucson, Arizona.  The event features police K-9 teams from around the region competing in area/building searches, an obstacle course, tactical obedience, handler protection scenarios (bite work), and narcotic/explosive detection.</p>
<p>Event administrators are working industriously to finalize the location, and more information will be forthcoming at <a href="http://aleca.policek9.com/TUCSONAREA.html" target="PPK9_external">aleca.policek9.com/TUCSONAREA.html</a>.</p>
</li>
' ; ?>

<?php if($today <= 20060229) print '
<li>
<p><b>Arizona Animal Fair</b> - February 18, 2006</p>
<p>Protect Police K9 returns this year to participate again in the Arizona Animal Fair sponsored by <b>S.A.F.E.</b>.  Please join at <a href="http://maps.yahoo.com/maps_result?ed=y.r5aep_0Tr.EXoNK5i52k1cgNhpKq_QZRrBEfw.tub7&csz=tucson%2C+az&country=us&new=1&name=&qty=" target="PPK9_external">Reid Park</a> in Tucson for live performances, demonstrations, vendor exhibits, food, and fun. Events include performances by the <a href="http://www.skyydogsusa.com/dreamteam.html" target="PPK9_external">Skyy Dogs USA Dream Team K-9s</a> and the <a href="http://www.frisbeek9.com/" target="PPK9_external">Muttley Crew</a>, animal painting, clinics, and more.</p>
<p>Additionally, we are seeking K9 units and handlers to participate in demonstrations.  Please contact <a href="mailto:gil@protectpolicek-9.com">Gil Valdez</a> if you are able to help out.</p>
<img src="images/events/arizonaanimalfair.gif" alt="Arizona Animal Fair logo" />
<p>For more information, visit <a href="http://www.safeanimals.com/aaf.html" target="PPK9_external">www.safeanimals.com/aaf.html</a>.</p>
</li>
' ; ?>

<?php if($today <= 20060410) print '
<li>
<p><b>4th Annual Desert Dog Police K9 Trials</b> - April 8-9, 2006</p>
<p>The <b>Arizona Law Enforcement Canine Association</b> (A.L.E.C.A.) invites you to attend the Desert Dog IV Police K-9 Trails, held April 8th and 9th of 2006 at <a href="http://local.google.com/local?f=q&hl=en&q=1235%20N%20Center%20St%2C%20Mesa%2C%20AZ%2085201&ll=33.437565,-111.831121&spn=0.017226,0.04313&sa=N&tab=wl" title="map to Hohokam Park" target="PPK9_external">Hohokam Park</a> in Mesa, Arizona.  The event features police K-9 teams from around the Southwest region competing in area/building searches, an obstacle course, tactical obedience, handler protection scenarios (bite work), and narcotic/explosive detection.  Admission is free.</p>
<img src="images/events/desertdogtrials.gif" alt="ALCEA Desert Dog Trials logo" />
<p>For more information, visit <a href="http://www.desertdogk9trials.com" target="PPK9_external">www.desertdogk9trials.com</a>.</p>
</li>
' ; ?>

<?php if($today <= 20071031) print '
<li>
<p><b>WalMart National Safety Month</b> - October 2007</p>
<p>October is National Safety Month for WalMart;  PPK9 will hold fundrasier events each weekend of the month at local WalMart locations.  Police K-9 officers will be present to give demonstrations, and PPK9 representatives will be available to answer questions and facilitate your support for our programs.</p>
</li>
' ; ?>

<?php if($today <= 20070217) print '
<li>
<p><b>6th Annual Arizona Animal Fair</b> - February 17th, 2007</p>
<p>Please join us on Saturday, <strong>February 17th</strong> from <strong>10am to 5pm</strong> as Protect Police K9 returns to participate in the Sixth Annual Arizona Animal Fair at <a href="http://maps.yahoo.com/maps_result?ed=y.r5aep_0Tr.EXoNK5i52k1cgNhpKq_QZRrBEfw.tub7&csz=tucson%2C+az&country=us&new=1&name=&qty=" target="PPK9_external">Reid Park</a> in Tucson.</p>
<p>The Arizona Animal Fair (AAF) is an annual festival attended by over 8000 people (and their pets).  The fair features six arenas of non-stop performances (including frisbee, flyball, agility, weigh-pulling, and doggie-dancing) and demonstrations by Police K-9 units and search-and-rescue dogs.  More than 100 exhibitor booths provide the opportunity to shop around, try the latest pet products, and learn from industry experts. There will be lots of great animals available for adoption as well!</p>
<p>This important community event benefits the entire Tucson community, not only by fostering collaboration among local animal rescue and welfare agencies, but also by educating pet owners about adoption, microchipping, and other emerging developments in animal care.  All proceeds are returned to the animal rescue community.</p>
<img src="images/events/arizonaanimalfair.gif" alt="Arizona Animal Fair logo" border="1" />
<p>For more information, visit <a href="http://www.safeanimals.com/aaf.html" target="PPK9_external">www.safeanimals.com/aaf.html</a>.</p>
</li>
' ; ?>

<?php if($today <= 20070415) print '
<li>
<p><b>5th Annual Desert Dog Police K-9 Trials</b> - April 14-15, 2007</p>
<p>The <strong>Arizona Law Enforcement Canine Association</strong> (A.L.E.C.A.) invites you to attend the <strong>Desert Dog V Police K-9 Trails</strong>, held April 14th and 15th the newly renovated <a href="http://www.google.com/maps?f=q&hl=en&q=Scottsdale+Stadium+7408+E+Osborn+Rd+85251&ie=UTF8&cid=33493559,-111916697,11999534378130834979&li=lmd&z=14&om=1" title="map to Scottsdale Stadium" target="PPK9_external">Scottsdale Stadium</a> in Scottsdale, Arizona.  The event features police K-9 teams from around the Southwest region competing in area/building searches, an obstacle course, tactical obedience, handler protection scenarios (bite work), and narcotic/explosive detection.  Admission is free.</p>
<img src="images/events/desertdogtrials.gif" alt="ALCEA Desert Dog Trials logo" />
<p>For more information, visit <a href="http://www.desertdogk9trials.com" target="PPK9_external">www.desertdogk9trials.com</a>.</p>
</li>
' ; ?>

<?php if($today <= 20080217) print '
<li>
<p><b>7th Annual Arizona Animal Fair</b> - February 16th, 2008</p>
<p>Please join us on Saturday, <strong>February 16th</strong> from <strong>10am to 4pm</strong> as Protect Police K9 returns to participate in the Seventh Annual Arizona Animal Fair at <a href="http://maps.yahoo.com/maps_result?ed=y.r5aep_0Tr.EXoNK5i52k1cgNhpKq_QZRrBEfw.tub7&csz=tucson%2C+az&country=us&new=1&name=&qty=" target="PPK9_external">Reid Park</a> in Tucson.</p>
<p>The event is <strong>free</strong> for people and their pets, and all proceeds from the activities are returned to the animal rescue community.</p>
<p>The Arizona Animal Fair, now in its seventh year in Tucson, features entertainment, educational demonstrations, fun activities for people and pets, plenty of pet-related vendors, food, a beer garden, and hundreds of animals available for adoption.</p>
<p>This important community event benefits the greater Tucson community by encouraging collaboration among nonprofit animal rescue and welfare agencies as well as local and national businesses.  The Arizona Animal Fair also advances community health and welfare interests by actively supporting the reduction of animal euthanasia through education, adoption, vaccination and microchipping.</p>
<img src="images/events/arizonaanimalfair.gif" alt="Arizona Animal Fair logo" border="1" />
<p>For more information, visit <a href="http://www.azanimalfair.com" target="PPK9_external">www.azanimalfair.com</a>.</p>
</li>
' ; ?>

<?php if($today <= 20080414) print '
<li>
<p><b>6th Annual Desert Dog Police K-9 Trials</b> - April 12-13, 2008</p>
<p>The <strong>Arizona Law Enforcement Canine Association</strong> (A.L.E.C.A.) invites you to attend the <strong>Desert Dog V Police K-9 Trails</strong>, held April 12th and 13th the newly renovated <a href="http://www.google.com/maps?f=q&hl=en&q=Scottsdale+Stadium+7408+E+Osborn+Rd+85251&ie=UTF8&cid=33493559,-111916697,11999534378130834979&li=lmd&z=14&om=1" title="map to Scottsdale Stadium" target="PPK9_external">Scottsdale Stadium</a> in Scottsdale, Arizona.  The event features police K-9 teams from around the Southwest region competing in area/building searches, an obstacle course, tactical obedience, handler protection scenarios (bite work), and narcotic/explosive detection.  Admission is free.</p>
<img src="images/events/desertdogtrials.gif" alt="ALCEA Desert Dog Trials logo" />
<p>For more information, visit <a href="http://www.desertdogk9trials.com" target="PPK9_external">www.desertdogk9trials.com</a>.</p>
</li>
' ; ?>

<?php if($today <= 20100220) print '
<li>
<p><b>9th Annual Arizona Animal Fair</b> - February 20th, 2010</p>
<p>Please join us on Saturday, <strong>February 20th</strong> as Protect Police K9 returns to participate in the Ninth Annual Arizona Animal Fair at <a href="http://maps.yahoo.com/maps_result?ed=y.r5aep_0Tr.EXoNK5i52k1cgNhpKq_QZRrBEfw.tub7&csz=tucson%2C+az&country=us&new=1&name=&qty=" target="PPK9_external">Reid Park</a> in Tucson.</p>
<p>The event is <strong>free</strong> for people and their pets, and all proceeds from the activities are returned to the animal rescue community.</p>
<p>The Arizona Animal Fair, now in its ninth year in Tucson, features entertainment, educational demonstrations, fun activities for people and pets, plenty of pet-related vendors, food, a beer garden, and hundreds of animals available for adoption.</p>
<p>This important community event benefits the greater Tucson community by encouraging collaboration among nonprofit animal rescue and welfare agencies as well as local and national businesses.  The Arizona Animal Fair also advances community health and welfare interests by actively supporting the reduction of animal euthanasia through education, adoption, vaccination and microchipping.</p>
<img src="images/events/arizonaanimalfair.gif" alt="Arizona Animal Fair logo" border="1" />
<p>For more information, visit <a href="http://www.azanimalfair.com" target="PPK9_external">www.azanimalfair.com</a>.</p>
</li>
' ; ?>

<?php if($today <= 20120427) print '
<li>
<p><b>Sechrist Elementary School vest presentation</b> - Friday, April 27, 2012</p>
<p>PPK9 and Sechrist Elementary School will present a bullet/stab resistant vest to AZ D.P.S. K-9 "Leo" around 10am at the school. K9 Leo patrols the Flagstaff-area highways.</p>
<p><strong>10:00am</strong><br />
2230 N Fort Valley Road<br />
Flagstaff, Arizona</p>
</li>
' ; ?>

<?php if($today <= 20120505) print '
<li>
<p><b>Queen Creek 4H vest presentation</b> - Saturday, May 5, 2012</p>
<p>PPK9 and Queen Creek 4H Club will present the Maricopa Sheriff office K-9 "YASS" his very own bullet/stab resistant vest.</p>
<p><strong>2:00pm</strong><br />
2661 East Dessert Lane<br />
Gilbert, Arizona</p>
</li>
' ; ?>

<?php /* Here begins the current stuff */ ?>

<?php if($today <= 20130302) print '
<li>
<p><b>12th Annual Arizona Animal Fair</b> - March 2nd, 2013</p>
<p>Please join us on Saturday, <strong>March 2nd</strong> as Protect Police K9 returns to participate in the Twelfth Annual Arizona Animal Fair in Tucson.</p>
<p>The event is <strong>free</strong> for people and their pets, and all proceeds from the activities are returned to the animal rescue community.</p>
<p>The Arizona Animal Fair, now in its twelfth year in Tucson, features entertainment, educational demonstrations, fun activities for people and pets, plenty of pet-related vendors, food, a beer garden, and hundreds of animals available for adoption.</p>
<p>This important community event benefits the greater Tucson community by encouraging collaboration among nonprofit animal rescue and welfare agencies as well as local and national businesses.  The Arizona Animal Fair also advances community health and welfare interests by actively supporting the reduction of animal euthanasia through education, adoption, vaccination and microchipping.</p>
<img src="images/events/arizonaanimalfair.gif" alt="Arizona Animal Fair logo" border="1" />
<p>For more information, visit <a href="http://www.azanimalfair.com" target="PPK9_external">www.azanimalfair.com</a>.</p>
</li>
' ; ?>

<?php if($today <= 20160127) print '
<li>
<p><b>Adopt Love Adopt Local event</b> - April 16th, 2016</p>
<p>Please join us on Saturday, <strong>April 16th</strong> as Protect Police K9 returns to participate in the Adopt Love Adopt Local event at the Tucson Expo.</p>
<p>Adopt Love Adopt Local is a one-day mega-adoption animal fair designed to help hundreds of our community’s homeless pets find their forever homes.</p>
<p>The inaugural event was a <strong>HUGE</strong> hit with 2300 attendees and 130 animals adopted that day. The event will run from 9AM to 4PM and will feature demonstrations from the Border Patrol, the Sheriff department, and the local Police department with their K-9 companions.</p>
<p>Each department uses and presents the dogs slightly different from each other, so be sure to catch them all, presentations are throughout the day, taking place at 10AM, 12 noon, and 2PM.</p>
<img src="images/events/adoptloveadoptlocal.jpg" alt="Adopt Love Adopt Local logo" border="1" />
<p>For more information, visit <a href="http://adoptloveadoptlocal.org" target="PPK9_external">www.adoptloveadoptlocal.org</a>.</p>
</li>
' ; ?>


</ul>

<p><?
echo 'This listing was last updated on <b>' ;
echo $date ;
echo '</b>.'
?></p>

</div>

<?

include 'includes/footer.php' ;

?>
