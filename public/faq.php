<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>

<div class="maindiv">

<h1>Frequently Asked Questions</h1>

<p><b>1. Who is Protect Police K-9?</b></p>
<p>Protect Police K-9 is an Arizona non-profit organization lead by founder <a href="directors.php">Michael Valdez</a> and a diverse <a href="directors.php">Board of Directors</a>, who work tirelessly to protect Arizona's Police dogs by providing K9 ballistic vests to law enforcement units.</p> 
<p><b>2. How much do those vests cost?</b></p>
<p>Each vest is custom-built using high-end ballistic material and costs about $825.</p>
<p><b>3. Is it really worth the expense? After all, they're just dogs, right?</b></p>
<p>Police K9 aren't <i>just</i> dogs. Any K9 handler will attest that a K9 counterpart is no less valuable than a human partner - Police dogs are well-trained, intelligent, enthusiastic, and loyal.  As law enforcement becomes increasingly dangerous, agencies are relying more and more on the agility, versatility, speed, stamina, and senses of Police K9 to protect officers and citizens. Given the substantial investment of time and money in each dog's training and care, and their contribution to the success of law enforcement, it makes sense to protect them as they serve our community.</p>
<p><b>4. I have an old bullet proof K9 vest; can I donate it?</b></p> 
<p>The vests we present to law enforcement units are custom-manufactured specifically for each K9 we sponsor, and are presented in factory-new condition.  However, you are welcome to contact your local K9 unit to inquire about their need and requirements for K9 vest donations.</p>
<p><b>5. I am a K9 handler; how can I request a vest for my unit/canine?</b></p> 
<p>Units and handlers should refer to the <a href="request.php">Request Page</a> for information about requesting PPK9 sponsorship.</p>
<p><b>6. What if my local Police department doesn't have a K9 squadron?</b></p>
<p>Police departments which do not have a K9 squadron receive coverage from another local, county, or state K9 division.</p>
<p><b>7. How much of my donation will actually go to Arizona Protect-a-Dog (a.k.a. Protect Police K-9)?</b></p>
<p><a href="http://www.protectpolicek-9.com">Arizona Protect-a-Dog, Inc.</a> is a 501(c)3 non-profit organization and will receive 100% of your donation. In addition, all donations are tax-deductable.</p>
<p><b>8. What is my donation money used for?</b></p>
<p>Our budget covers the purchase of K9 vests, hosting of fund-raising events, and associated administrative, transportation, and legal expenses.</p>
<p><b>9. I can't donate cash; do you accept donations of anything else?</b></p>
<p>PPK9 uses lots of stamps (for fund-raisers and other mailings), and also accepts donations of dog-treats (given to dogs at vest presentations, fund-rasiers, and educational demonstrations).</p> 
<p><b>10. How else can I help?</b></p>
<p>The <a href="help.php">How to Help</a> page provides information about all the various ways you can contribute to our mission.  For example, PPK9 is in search of grant writers to donate their services; interested parties should contact <a href="mailto:reagen.kulseth@protectpolicek-9.com" title="Reagen.Kulseth [at] ProtectPoliceK-9.com">Reagen Kulseth</a> for more information.</p>
<p><b>11. What if I don't live in Arizona?</b></p>
<p>You are welcome to support Arizona's Protect-a-Dog, and may also look among our <a href="links.php">useful links</a> to find a Vest-a-Dog organization in your area.</p>
<p><b>12. What if there isn't a Protect-a-Dog organization in my state and I want to start one?</b></p>
<p>You can get information about fund-raising and K9 vests from PPK9's <a href="mailto:michael.valdez@protectpolicek-9.com" title="Michael.Valdez [at] ProtectPoliceK-9.com">Michael Valdez</a>, or you can contact the National Vest-a-Dog office at <a href="http://www.dogvest.com">www.dogvest.com</a>.</p>
<p><b>13. Will you put a link to my website in your Useful Links section?</b></p>
<p>If your website is relevant to our mission, contact the webmaster, <a href="mailto:ppk9@x.michaelrog.com" title="Email the webmaster">Michael</a>, for information about including a link to your website in our <a href="links.php">Useful Links section</a></p> 

</div>

<?php

include 'includes/footer.php' ;

?>