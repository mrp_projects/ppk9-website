<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>


<div class="maindiv">

<h1>The K-9 Pledge</h1>

<p>

I promise to be worth every cent of the $10,000 it took to train me.<br />
I promise to track down criminals who threaten and harm our community.<br />
I promise to stand with you against the violence that penetrates our society. <br />
I promise to sniff out the drugs being advertised to your children.<br />
I promise to stay resolved and focused upon the success of our mission.<br />
<br />
No matter what happens, I will be by your side...<br />
your loyal companion until the end.<br />
I will never give up.
</p>


<h1>Arizona Police Dogs Killed in Action</h1>

<p>
Presented here is a list of the brave police dogs who have died in service to the community:</p>

<ul><li>
  <p><b>Phoenix PD - K-9 Woo, K.I.A. November 23, 1974</b></p>
  <p>Police dog Woo was killed during an East Side burglary call when he was struck by a patrol car arriving on the scene. Woo was struck at East Speedway and North Holly Ave. K9 handler, Ofc. James Richards had dropped the animal's leash to let him cool off after he had searched in vain for a burglar near the intersection.</p>
</li>
<li>
  <p><b>Maricopa County Sheriff's Office - K-9 Sam, died in 1977</b></p>
  <p>K9 handler Garland Moore's police dog Sam was deliberately poisoned in its backyard.</p>
</li>
<li>
  <p><b>Phoenix PD - K-9 Roscoe, K.I.A. July 13, 1984</b></p>
  <p>On July 13, 1984, Ofc. Jeff Fenton stopped a drunk driver. Ofc. Fenton attempted to take the driver into custody when a struggle began. Roscoe jumped out of the patrol car to assist Ofc Fenton. Ofc, Fenton took control of the suspects with the assistance of K9 Roscoe. Ofc. Fenton ordered Roscoe to go back to the patrol car when he was struck by a passing vehicle.</p>
</li>
<li>
  <p><b>Phoenix PD - K-9 Yeager, died in 1986</b></p>
  <p>K9 Yeager died due to injuries he received during a training exercise. Yeager fell from a second floor and broke his back.</p>
</li>
<li>
  <p><b>Tempe PD - K-9 Murph, K.I.A. November 21, 1986</b></p>
  <p>On November 21, 1986, an armed suspect who had shot a Scottsdale PD Officer and injured several citizens in Scottsdale fled into Tempe. During the pursuit, the suspect kidnapped six year old Robbie Hughes as he was walking home from Meyer School, and held him hostage inside the home of 1254 E. Malibu. At one point in the confrontation, circumstance made it necessary for Sgt. Tranter to enter the home to attempt the rescue of the little boy. Upon entering the house, Sgt Tranter was shot in the face with a glaser round. He was unable to immediately return fire because he was unaware of the location of the hostage. Once it was determined that Robbie was in another part of the house and away from the suspect Sgt. Tranter returned fire and located Robbie. Sgt Tranter's radio was also struck by a .44 caliber round fired by the suspect. Sgt. Tranter was able to drag the hostage to safety. Immediately after Robbie's rescue K9 Officer Carlos Araiza sent his police dog Murph into the home to confront the suspect while other members of the tactical team forced entry through the front door. Murph was shot by the suspect and later died. Officers Les Gray, Gary Lindberg and Tom Stubbs entered the dark home through the front door. Officer Gray encountered the suspect in the hallway. The suspect fired one round from a handgun, which struck Officer Gray in the right hand and right bicep. Officer Gray returned fire, striking the suspect in the chest. The suspect died from his wounds.</p>
</li>
<li>
  <p><b>Maricopa County Sheriff's Office - K-9 Ado, died in 1987</b></p>
  <p>Deputy Mark Kentra was investigating an incident at the Salt River and was away from his patrol vehicle. K9 Ado jumped up in the front seat where he accidentally shut the car off. Deputy Mark Kentra was away from his patrol car for approximately 5 minutes. When he returned he found the K9 Ado was having heat stroke. Paramedics at the River worked immediately on Ado. He then was flown out by MCSO helicopter to the nearest vet. The vet was unable to save Ado from the heat stroke.</p>
</li>
<li>
  <p><b>Phoenix PD - K-9 Dax, K.I.A. Oct. 20, 1994</b></p>
  <p>On October 20, 1994, Officer Ben Boyer responded to 1721 W. Carson to assist the Phoenix Special Assignment Unit with the apprehension of a homicide suspect barricaded in his home. K9 Dax was deployed to locate the suspect in the residence and was shot by the suspect using a 12-gauge shotgun. The suspect later shot at and injured SAU officers. The suspect ultimately died of injuries sustained during the shooting with the police.</p>
</li>
<li>
  <p><b>Tucson PD - K-9 Dax, wounded January 20, 1995</b></p>
  <p>On January 20th 1995 at 10:00pm , Tucson Police K-9 officer Jennifer Willis-Stokes and her K-9 Dax were flagged down by the victim of an armed robbery involving a handgun. Some of the victims friends followed the suspect's vehicle, a blue and white van. Officer Willis-Stokes quickly located the suspect's vehicle and gave chase. Other units, including the police helicopter, were also involved in the pursuit. At one point, the violently out-of-control suspect tried to ram a patrol car.</p>
  <p>After crashing his vehicle, the suspect bailed out and started running. Officer Willis-Stokes and Dax were right behind him. The defendant tried to break into a residence, but the officers were too close. He turned and looked directly into the officer's eyes. He started to reach for his gun. Officer Willis-Stokes stopped and drew her gun. Dax continued toward the suspect, jumping up to grab his arm. It was a critical juncture that the police helicopter closed in casting a shadow that plunged the scene into complete darkness. The only thing Officer Willis-Stokes could see next was the muzzle flash of the suspect's gun.</p>
  <p>The officer returned fire, aiming toward the the muzzle flash since she could see nothing else. As the helicopter moved aside, the scene was bathed in an eerie light which revealed Dax laying on the ground struggling in a pool of blood. He had been shot in the head.</p>
  <p>The officer jumped behind a small tree. The suspect, who had been struck in the hip by the officer's bullet, crawled under a pine tree in the front yard of the residence. The officer could see Dax struggling and bleeding heavily, but she could not go to him until the suspect was contained. Remarkably, Dax dragged himself another twenty feet toward the suspect trying to get back in the fight to save his partner.</p>
  <p>Within minutes, other officers arrived and the suspect eventually surrendered. Officer Willis-Stokes immediately ran to dax who was flinging his head, struggling to rise. The officer found the bullet hole in Dax's head about half way between his right eye and ear. She knew Dax needed help fast. Air1 immediately landed near-by. Dax was carried to the helicopter by make-shift stretcher, During the short flight to the emergency veterinary clinic, Officer Willis-Stokes held Dax to her chest. Blood was running down her chest from Dax's head wound. She grieved because she did not expect Dax to live.</p>
  <p>Once at the veterinary clinic, Dax was stabilized. Remarkably, the suspect's 9mm bullet missed Dax's cranium. The bullet made a hole in his jawbone, traveled down his neck and lodged in his stomach. Surgery was performed to repair the damage. Dax did not emerge unscathed, however. He has severe nystagmus, balance problems, and hearing loss in one ear.</p>
  <p>After two months of recovery, Dax returned to duty. He was later awarded the Scarlet Shield, an honor generally reserved for officers of the two-legged variety. Upon learning of Dax's story, 8-year old Michael Valdez decided to take action to protect police dogs from injury in the line of duty - he founded <a href="http://www.protectpolicek-9.com">Arizona's Protect Police K-9.</a></p>
</li>
<li>
  <p><b>Phoenix PD - K-9 Hunter, K.I.A. April 17, 1996</b></p>
  <p>On April 17, 1996 at 2:00 AM, K9 Officer Jeff Kozel and Hunter responded to the area of 3000 E. Cactus Rd. to a subject with a gun call. The subject was located in the backyard of a residence attempting to force entry. K9 Hunter was deployed into the backyard to apprehend the suspect and to allow officers enough time to evacuate the sleeping family. K9 Hunter was shot five times by the suspect and returned to Officer Kozel when called. K9 Hunter died in Officer Kozel's arms en route to the hospital. The suspect was arrested and convicted.</p>
</li>
<li>
  <p><b>University of Arizona PD - K-9 Jos, died in 1996</b></p>
  <p>Officer Mike Thomas and his K9 partner Jos had conducted a drug search in Tucson for Tucson PD. They did not know at the time that it was a meth lab. Shortly after, in 1996 during the Clark Peak fire on Mt. Graham, K9 Jos was exposed substantial amounts of smoke. There were major breathing problems and Jos died very quickly. It was discovered afterwards that there was scaring in his lungs attributed to the meth lab search leading to his death.</p>
</li>
<li>
  <p><b>Arizona DPS - K-9 Ringo, died August 12, 1999</b></p>
  <p>Officer Chris Hemmen let his K9 partner Ringo out for a break off leash along State Route 77. K9 Ringo ran out into the highway and was hit by a semi truck.</p>
</li>
<li>
  <p><b>Arizona DPS - K-9 Hugo, died in 2001</b></p>
  <p>Officer Bart Massey was attending EOD class for the department. He left his K9 partner Hugo in the vehicle with the air condition running. After approx. 40 minutes Ofc. Massey went out to check on his partner and found that the air conditioner in the vehicle had stopped running. K9 Hugo died in the back of the car of possibly heat stroke.</p>
</li>
<li>
  <p><b>Pima County Sheriff's Office - K-9 Rico, K.I.A. July 20, 2004</b></p>
  <p>Deputy Rodney Hamilton and K9 Rico assisted on a search for a suspect that fled from deputies during a pursuit in the area of Green Valley, AZ. The suspect had bailed from a stolen motorcycle and fled on foot into the desert. After 4-5 minutes of searching K9 Rico gives a strong alert. Deputy Hamilton deploys the dog. K9 Rico heads off into the brush. After a minute or so the deputies hear a loud thump near the freeway. A passing car struck K9 Rico. It appeared that the suspect had doubled back and crossed the freeway with K9 Rico not far behind him. K9 Rico died at the scene.</p>
</li>
<li>
  <p><b>Avondale PD - K-9 Rex, died July 22, 2004</b></p>
  <p>Officer Kris Guffy's K9 partner, Rex, died during emergency surgery. On July 22, 2004 the Avondale Police Department Canine Unit was doing in-service training. While doing a scenario K9 Rex received a laceration to the top of his nose that had to be sutured. Upon arrival to the Emergency Animal Clinic K9 Rex was checked out and the vet agreed that he would need sutures. K9 Rex was given anesthesia and taken into surgery. K9 Rex stopped breathing during the surgery. The Emergency Animal Clinic tried CPR but was unable to revive Rex.</p>
</li>
</ul>

<ul>
  <li>
    <p><b>Tohono O'odham Police Department - K-9 Yari, died February 2005</b></p>
    <p align="justify">Officer Ed Perez of the Tohono O'odham Police Department, lost his partner Yari. While Officer Perez had his vehicle sitting in the parking lot of their station, the AC malfunctioned. Yari has been with the K-9 unit since 2000. Among his many achievements were 1st place narcotics detection 2001 Tucson area K-9 Trials. His largest marijuana load was 1800 lbs, the smallest amount found was one seed under the rear seat of a vehicle. Yari had detected over 20,000 lbs of marijuana on traffic stops. Within the last month, Yari had three marijuana loads and an alert on a false bed for Border patrol that yielded 571 lbs of cocaine. Yari also had eight patrol apprehensions. He a was an asset to the Tohono O'odham Police Department K-9 Unit and will be missed by Officer Perez, his family and the K-9 Unit.</p>
  </li>
  <li>
    <p><b>Arizona DPS - K-9 Barry, died May 17, 2005</b></p>
    <p align="justify">On May 16, 2005, DPS Officer Marty Lepird and his K9 partner, Barry, conducted training with the department's SWAT team in Phoenix. The next morning, Barry suffered internal bloating and died in his kennel. Among other distinctions, K9 Barry was assigned to protect President Bush during the 2004 Presidential Campaign<strong>.</strong></p>
  </li>
  <li>
    <p><b>Phoenix Police Department - K-9 R.J., died August 13, 2005</b> </p>
    <p align="justify">On Saturday August 13th, 2005, at approximately 1830 hours, several officers from the Central City Precinct were pursuing a suspect for armed robbery. Officer Bryan Hanania and his police service dog R.J., were trying to apprehend the suspect after he fled from his vehicle on foot in the neighborhood of 2100 East Garfield. Officer Hanania and other officers chased the suspect into an alley where the suspect had reacquired his vehicle. As the officers were in the alley, the suspect drove rapidly in their direction. The suspect appeared to intentionally drive toward R.J. and struck the K-9 with his vehicle. The officers were able to avoid being struck by the car and the suspect was eventually apprehended by other officers.Officers immediately transported R.J. to an emergency clinic where he was diagnosed with critical injuries to include a broken spine. R.J. was euthanized as his injuries were determined to be insurmountable. R.J. was a three year old Belgium Malinois who was with Officer Hanania and the Phoenix Police Department's Canine Unit as a team for 18 months. Officer Hanania and R.J. have deployed over three hundred times to assist officers in locating dangerous suspects and/or illegal narcotics.</p>
  </li>
</ul>
<ul>
  <li>
    <p><b>Mohave County Sheriff's Office - K-9 Rocky, died November, 2005</b></p>
    <p align="justify">Deputy Raja Karim and his K-9 partner &quot;Rocky&quot;, were assigned to the Kingman-Golden Valley area. Deputy Karim and &quot;Rocky&quot; had worked together since 2004. Deputy Karim and K-9 Rocky had just completed a training session when, on their way out of a two story building they had just completed doing a search in, Deputy Karim observed K-9 Rocky suddenly start to hold his right leg up and howl on the stairwell. There were no signs of injury during the training scenario and K-9 Rocky had a great bite. Deputy Karim attended to Rocky and took him to the vet. It was determined that he had a broken leg and surgery was needed. Rocky passed away during the surgery. Rocky will be greatly missed by his family, the Mohave County Sheriff's Office, and all that knew and loved him. </p>
  </li>
  <li>
    <p><b>Tucson Police Department - K-9 Miko, died May 28, 2006</b></p>
    <p align="justify">On May 28th, 2006, Officer Schad and his K-9 partner Miko, were hot on the trail of a suspected carjacker. K-9 Miko was deployed to catch the suspect who had bailed on foot. K-9 Miko pursued the suspected carjacker, who then jumped from an overpass at Kino and Aviation Parkways with K-9 Miko following. While the suspect was able to get away, K-9 Miko took a 30-foot-fall, breaking his back, shattering a leg and suffering neurological damage. He was euthanized as a result of his injuries. K-9 Miko has been cremated and his handler, Officer Gary Schad, said he, his wife and their 11-year-old son will decide in about a year where to spread his ashes. A memorial service honoring Tucson's fallen hero, K-9 Miko, was held on May 14th, 2006, at Reid Park in Tucson. Present at the services were the Commanders from the Tucson Police Department, numerous K-9 Officers and representatives from Valley Animal Hospital and other animal organizations, who spoke at the ceremony. A tree planting ceremony also took place to honor K-9 Miko.</p>
  </li>
  <li>
    <p><b>Mesa Police Department - K-9 Leon, died March 20, 2007</b></p>
    <p align="justify"> On March 20th, 2007, at approximately 22:15 hours, Officer Lafontaine detained a suspicious person on a bike in the K-Mart Parking Lot, located on the northwest corner of Lindsay Road and Main Street, in Mesa. Upon learning that the 19 year old detained subject was wanted on outstanding warrants for burglary and a probation violation, Officer Lafontaine attempted to take the suspect into custody with the suspect resisting arrest. The suspect assaulted Officer Lafontaine by throwing his bicycle at him, then proceeded to run south in the parking lot towards Main Street. Officer Lafontaine then deployed K-9 Leon to apprehend the fleeing suspect, who continued to run south through the parking lot and then proceeded to cross the street. K-9 Leon continued on in pursuit of the suspect and as he ran south across Main Street, he was struck by a car. Officer Lafontaine rushed over to his K-9 partner, placed him in his vehicle and proceeded to rush him to the veterinarian hospital, where K-9 Leon was pronounced dead on arrival. The suspect was later apprehended by Mesa Police Officers. In addition to being booked on the outstanding warrants, the suspect was also booked on suspicion of aggravated assault, resisting arrest and false reporting. K-9 Leon is remembered not only for his courageousness and his agility, but also for his playful spirit. He is missed by his handler and his family, whom he resided with when he wasn't on duty, members of the Mesa K-9 Unit and by members of the Mesa Police Department.</p>
  </li>
  <li>
    <h1><font size="4" face="Arial, Helvetica, sans-serif">Police Dog Memorial To Honor K-9s Killed in The Line of Duty</font></h1>
    <p align="justify" class="outdated"> <font color="#000000">Gordon Leitz, of The Arizona Law Enforcement Canine Association, has spearheaded a project to erect a Memorial dedicated to Police Service Dogs from Arizona, that have lost their lives in the line of duty. The Arizona Police K-9 Memorial is a life-size bronze German Shepherd, standing on a granite base, with the names etched on the Memorial of those canines who have been killed in the line of duty. The K-9 Memorial has been placed at Wesley Bolin Plaza, near the Arizona Police Officer Memorial. The K-9 Memorial was built at a cost of approximately $40,000, all thanks to the generous support of donors. No state funds were allowed to be used for the Memorial. You are welcome to make donations to assist in the annual grounds maintenance fees, the cost of quarterly cleaning of the Memorial, and also the costs to add names of future canines lost in the line of duty. For further information about The Arizona Police K-9 Memorial, please contact Gordon Leitz, at <a href="../../public_html/gordy@azk9memorial.net">gordy@azk9memorial.net</a>, or Owen Keefe, at <a href="../../public_html/owen@azk9memorial.net">owen@azk9memorial.net</a>, or visit the web site at <a href="http://www.azk9memorial.net" target="_blank">www.azk9memorial.net</a></font></p>
  </li>
</ul>
</div>

<?php

include 'includes/footer.php' ;

?>