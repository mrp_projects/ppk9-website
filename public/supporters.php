<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>


<div class="maindiv">

<h1>Sponsors and Supporters</h1>

<p>
Protect Police K-9 would not be able to achieve success if not for the generous support of our sponsors.  We encourage you to patronize their services, as they have faithfully supported our mission to vest Arizona's Police dogs.
</p>

<hr />

<div class="block">

<img src="images/supporters/walmart.gif" class="bod" alt="Walmart's corporate citizenship in our community has been instrumental to our success." />

<p>
<img src="images/supporters/walmart-logo.gif" alt="Walmart logo"></p>

<p>
<b>Wal&bull;Mart</b> is one of Protect Police K-9's most involved supporters, contributing more than $13,000 to date.  Their assistance through grants, services, and fundraising efforts has been instrumental to our success.  Walmart's good corporate citizenship and generous support of our mission are greatly appreciated. More information about the <i><b>Good. Works.</b></i> program is available online at <a href="http://www.walmartfoundation.org/" target="PPK9_external">www.WalmartFoundation.org</a>.
</p>

</div>

<hr />

<div class="block">

<img src="images/supporters/shopper.gif" class="bod" alt="Diane Paulson of the Tuscon Shopper poses with a 25 year anniversary poster." />

<p>
<b>Diane Paulson, <i>The Tucson Shopper</i></b></p>

<p>
Ms. Paulson supplies Protect Police K-9 with pro-bono advertising in <i>The Tucson Shopper</i>.  We have received very positive responses to our ads in <i>The Tucson Shopper</i> and greatly appreciate Ms. Paulson's generosity.
</p>

<p>Please visit The Tucson Shopper at <a href="http://www.tucson-shopper.com" target="PPK9_external">www.Tucson-Shopper.com</a>.
</p>

</div>

<hr />

<div class="block">

<img src="images/supporters/oasis.gif" class="bod" alt="Left to right - Oasis employees Ronnie Stamps, Dan Poore, Bret Eastburn, Protect Police K-9 founder Michael Valdez, and K-9 Sara" />
<p>

<b>Dan Poore, Oasis Bottled Water</b></p>

<p>
An ongoing supporter of Protect Police K-9, Mr. Poore is a firm believer in our mission to provide protective vests for Police K-9s.  Mr. Poore began assisting Protect Police K-9 with fundraisers in March of 2002.  K-9 Sara (pictured) is one of two dogs to be vested thanks to Mr. Poore's generous efforts.  
</p><p>
Our thanks go out to Mr. Poore, Ronnie Stamps, Bret Eastburn, and all the staff at Oasis Bottled Water for their participation in our ongoing mission. Please visit Oasis Bottled Water's website at <a href="http://www.oasish2o.com" target="PPK9_external">www.oasish2o.com</a>.</p>

</div>

<hr />

<div class="block">

<img src="images/supporters/riggsroad.gif" class="bod" alt="Riggs Road Vet staff" />
<p>

<b>Riggs Road Veterinarians</b></p>

<p>
Riggs Road Veterinarians are good friends and ongoing supporters of Protect Police K-9.
</p>

<p>Visit them online: <a href="http://www.riggsroadvet.vetsuite.com/Templates/GridCritters.aspx" target="PPK9_external">Riggs Road Vet website</a></p>

</div>

<hr />

<div class="block">

<img src="images/supporters/tammymccullagh.gif" class="bod" alt="Tammy McCullagh" />
<p>

<b>Tammy McCullagh</b></p>

<p>Tammy McCullagh currently holds the title of Mrs. Tempe Amercia 2010 and was a contestant in the Mrs. Arizona America 2010 pageant. Prior to her current occupation &mdash; mother of two children &mdash; Tammy worked as a veterinary technician and pharmaceutical sales rep for veterinarians. She has over 15 years of experience in the veterinary industry. As a community servant and pageant contestant, Ms. McCullagh has been an outspoken supporter of PPK9's efforts.</p>

</div>

<hr />

<div class="block">

<img src="images/supporters/matthewrein.jpg" class="bod" alt="Matthew Rein" />
<p>

<b>Matthew Rein</b></p>

<p>Matthew Rein is a student at Esperero Middle School. He is an avid athlete: He plays basketball and tennis on school teams and participates in the Tucson Magic basketball club and JCC leagues.</p>

<p>Matthew is also a pet owner and animal supporter. When it came time for Matthew to decide on his mitzvah project (a good deed) for his upcoming Bar Mitzvah, he approached Protect Police K-9 to assist with <a href="http://www.orchadash-tucson.org/bnai-mitzvah/about-m-rein.html">fundraising efforts to vest Arizona's working dogs</a>. To date, his efforts have raised almost $2500 to purchase bullet/stab-proof vests.</p>

<p>(Matthew celebrated his Bar Mitzvah on June 12, 2010 at Congregation Or Chadash.)</p>

</div>

<hr />

<div class="block">

    <img src="images/supporters/kenAndJudy.jpg" class="bod" alt="Ken and Judy Wojtanek" />
    <p>

        <b>Ken and Judy Wojtanek</b></p>

    <p>Ken and Judy are originally from the Chicago area & spent their weekends in Michigan. While in Michigan they had the pleasure of always being visited by German Shepard dogs, that mainly belonged to farmers. They fell in love with them, and while watching K9 Cops on tv, felt they could help protect these wonderful courageous dogs. They currently reside in Anthem, AZ </p>

</div>

<hr />

<ul>


<li>S.A.F.E.</li>
<li>Tim Hope Academy of Martial Arts</li>
<li>Southern Arizona Veterinary Specialists</li>
<li>Pima County Sheriff Dept. - Det. Dan Preuss</li>

<li>Viki Watson, Ye Olde Clock Shoppe</li>

<li><a href="http://www.donmackey.com/en_US/" target="PPK9_external">Don Mackey G.M.C. Oldsmobile Cadillac</a></li>
<li><a href="http://www.holmestuttleford.com/" target="PPK9_external">Holmes Tuttle Ford</a></li>
<li><a href="http://www.jimclick.com/" target="PPK9_external">Jim Click Ford</a></li>
<li><a href="http://www.mcdonalds.com/" target="PPK9_external">McDonalds Corporation</a></li>
<li><a href="http://www.walmart.com/" target="PPK9_external">Wal-Mart</a></li>

<li>Pat Foremaster</li>
<li>Edna Phelps (In Loving Memory)</li>
<li>Univision T.V. (Carolina Fuentes)</li>

<li><a href="http://www.humane-so-arizona.org" target="PPK9_external">Southern AZ Humane Society (Amy Eades)</a></li>
<li><a href="http://www.avoncollectors.com" target="PPK9_external">Avon Collectors</a></li>
<li><a href="http://www.dovekeeper.com" target="PPK9_external">Dovekeeper Creative Service (Rosmarie Colombraro)</a></li>
<li><a href="http://www.bestdealsinamerica.net" target="PPK9_external">Best Deals of AZ</a></li>
<li><a href="http://www.tritronics.com" target="PPK9_external">Tri-Tronics Inc.</a></li>

<li>Finley Distribution Company (Ms. Dorthy Finley)</li>
<li>Holly Allen</li>
<li>Lorraine Inzalaco</li>
<li>K9 Connection (Shari Norton)</li>
<li><a href="http://www.legion.org" target="PPK9_external">American Legion, Ladies Auxiliary Post #7</a></li>
<li>US Air Force (Michael Sechler, Sgt. P. H. John)</li>
<li>Robinson Elementary School (Abe Aragon)</li>
<li><a href="http://www.kold.com" target="PPK9_external">KOLD Channel 13 (CBS)</a></li>
<li><a href="http://www.pantanoac.com" target="PPK9_external">Pantano Animal Clinic (Dr. M. Lent)</a></li>
<li>Benson Police Department</li>
<li><a href="http://www.ci.tucson.az.us/police/organization/support_services_/tactical_support_section/sdu_Main/sdu_main.htm" target="PPK9_external">Tucson Police Dept. K9 (Sgt. Carl Lewis)</a></li>
<li>Carolyn Smith</li>
<li><a href="http://www.tucsonvalleyanimalhospital.com/index.html" target="PPK9_external">Valley Animal Hospital</a></li>
<li><a href="http://www.pimasheriff.org" target="PPK9_external">Pima County Sheriff Dept. (Sgt. Pat McGhee)</a></li>
<li><a href="http://www.pimasheriff.org/index.html" target="PPK9_external">Pima County Sheriff Dept. (Clarence W. Dupnik Sheriff)</a></li>
<li>Campanion Animal Hospital</li>
<li>Broadcast Intelligent, Inc.</li>
<li><a href="http://www.tucsonsidewinders.com" target="PPK9_external">Tucson Sidewinders Baseball</a></li>
<li>Broadway Animal Hospital</li>
<li>Speedway Animal Hospital</li>
<li>Sahauro Vista Animal Clinic</li>
<li>VCA Animal Medical Clinic</li>
<li><a href="http://www.faircares.org/welcome/" target="PPK9_external">FAIR (Foundation for Animals In Risk)</a></li>
<li><a href="http://www.tucsonredcross.org/" target="PPK9_external">Southern American Red Cross</a></li>
<li>Tucson Education Association</li>
<li><a href="http://www.dm.af.mil/default.htm" target="PPK9_external">Davis Monthan Air Force Base</a></li>
<li>Franklin Mens Wear (George Franklin)</li>
<li><a href="http://www.angelcharity.org/" target="PPK9_external">Angel Charity For Children Inc.</a></li>
<li><a href="http://www.azstarnet.com/star/today/" target="PPK9_external">Arizona Daily Star</a></li>
<li><a href="http://www.tucsoncitizen.com/" target="PPK9_external">Tucson Citizen</a></li>
<li><a href="http://www.agedwards.com/" target="PPK9_external">A.G. Edwards &amp; Sons Inc.</a></li>
<li><a href="http://www.wspa.org.uk/index.php?ilocale=2" target="PPK9_external">World Society for the Protection of Animals (Karen Johnson)</a></li>
<li>Tucson City Council Members:<br />Fred Ronstadt (Ward VI), Kathleen Dunbar (Ward III), and Shirley Scott (Ward II)</li>
<li><a href="http://www.adc.state.az.us/" target="PPK9_external">Arizona Department of Corrections</a></li>
<li><a href="http://www.ci.tucson.az.us/ch12/" target="PPK9_external">Tucson 12 City Channel</a></li>
<li><a href="http://www.aphf.org/nacop.html" target="PPK9_external">National Association of Chiefs of Police (Jim Gordon)</a></li>
<li><a href="http://www.wb58.com/buzz.htm" target="PPK9_external">WB58, The Buzz (Joan Lee)</a></li>
<li><a href="http://www.vfw.org/" target="PPK9_external">Veterans of Foreign Wars of the United States</a></li>
<li>City of Tucson Office of the Mayor (Mayor Robert E. Walkup)</li>
<li><a href="http://www.ci.tucson.az.us/police" target="PPK9_external">City of Tucson Office of the Chief of Police (Cheif Richard Miranda)</a></li>
<li><a href="http://www.azvma.org/" target="PPK9_external">Arizona Veterinary Medical Association</a></li>
<li>Pima County Public Safety Association</li>
<li>Sahuaro High School student Lorene Nyberg</li>
<li><a href="http://www.weeangeltreats.com/" target="PPK9_external">Wild Angel Productions (Nancy Pouder)</a></li>
<li>Eegee's (Tina Ernle)</li>

</ul>


</div>

<?php

include 'includes/footer.php' ;

?>