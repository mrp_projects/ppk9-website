<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>

<div class="maindiv">

<h1>Useful Internet Links</h1>

<p>To request a vest for your K-9 or Unit, visit the <a href="request.php">request page</a>.</p>

<p><b>Protect Police K-9 in the News</b></p>
<p>PPK9 news features have moved to the <a href="news.php">News Archive</a>.

<p><b>Vest-a-Dog Organizations</b></p>
<ul>
<!-- dead links -- 
<li><a href="http://www.dogvest.com" target="PPK9_external">National Vest-A-Dog, Inc.</a> (founded by Stephanie Taylor)</li>
<li><a href="http://www.protectpolicek-9.com/" target="PPK9_external">Arizona Protect Police K-9</a> (run by Michael Valdez)</li>
<li><a href="http://www.protect-a-dog.com/" target="PPK9_external">California Vest Police K-9, Inc.</a> (run by Alyssa Mayorga)</li>
<li><a href="http://www.ctvestadog.org/" target="PPK9_external">Connecticut Vest-A-Dog, Inc.</a> (run by Christina Poryanda)</li>
<li><a href="http://www.ivestadog.org" target="PPK9_external">Illinois Vest-a-Dog</a> (run by Lee and Xavier Harrison)</li>
<li><a href="http://www.heroesonpaws.org/pages/1/index.htm" target="PPK9_external">Kentucky Vest-a-Dog</a> (run by Kathy and Stephanie Garcia)</li>
<li><a href="http://mainevestadog.homestead.com/" target="PPK9_external">Maine Vest-A-Dog Program</a> (run by Kelly Davis)</li>
-->
<li><a href="http://www.mavestadog.org/" target="PPK9_external">Massachusetts Vest-A-Dog Program</a> (run by Kathy and Lisa Hinds)</li>
<!-- dead links --
<li><a href="http://www.workingdog.org/k9vest.html" target="PPK9_external">New Hampshire Working Dog Foundation K-9 Vesting Program</a></li>
<li><a href="http://www.rivestadog.org/" target="PPK9_external">Rhode Island Vest-A-Dog Program</a> (run by Denise Anthony)</li>
-->
<li><a href="http://www.vestnpdp.com/" target="PPK9_external">Vest 'N P.D.P.</a> (run by Susie Jean)</li>
<!-- dead links --
<li><a href="http://www.gsdcofhouston.org/vest.html" target="PPK9_external">Texas The German Shepard Dog Club of Houston</a></li>
-->
</ul>
 
<p><b>Police Canine Associations</b></p>
<ul>
<li><a href="http://www.npca.net/" target="PPK9_external">National Police Canine Association</a></li>
<li><a href="http://www.uspcak9.com/" target="PPK9_external">United States Police Canine Association</a></li>
<li><a href="http://www.napwda.com/" target="PPK9_external">North American Police Work Dog Association</a></li>
<li><a href="http://www.alecapolicek9.com/" target="PPK9_external">The Arizona Law Enforcement Canine Association</a></li>
<li><a href="http://www.azk9memorial.net" target="PPK9_external">The Arizona Police K-9 Memorial</a></li>
<li><a href="http://www.desertdogk9trials.com" target="PPK9_external">Desert Dog Police K-9 Trials </a></li>
</ul>

<p><b>Working Dog Kennels and Training</b></p>
<ul>
<li><a href="http://www.nightwinds.com/" target="PPK9_external">Nightwinds International K9 Training</a> (Arizona)</li>
<li><a href="http://www.waddellkennels.com/" target="PPK9_external">Waddell Kennels</a> (Arizona)</li>
<li><a href="http://www.southwestworkingdogs.com/" target="PPK9_external">Southwest Working Dogs</a> (Arizona)</li>
<li><a href="http://www.adlerhorst.com/" target="PPK9_external">Alderhorst International K9 Academy</a> (California)</li>
<li><a href="http://www.allprok9training.com/" target="PPK9_external">All Pro K9 Training</a> (Florida)</li>
<li><a href="http://www.nodta.com/" target="PPK9_external">Northeast Ohio Dog Training Academy</a> (Ohio)</li>
</ul>

<p><b>Body Armor Manufacturing</b></p>
<ul>
<?php // (removed 12.6.2004 at request of Gil Valdez) <li><a href="http://www.internationalbodyarmor.com/Information/VestaDog/" target="PPK9_external">International Body Armor</a></li> ?>
<?php // (removed 10.4.2007 at request of Gil Valdez) <li><a href="http://www.armorshield.net/" target="PPK9_external">ArmorSheild</a></li> ?>
<li><a href="http://internationalarmor.net/k-9.html" target="PPK9_external">International Armor Corp.</a></li>
</ul>

<p><b>Arizona Law Enforcement Agencies</b></p>
<ul>
<li><a href="http://www.tucsonpolicefoundation.org" target="PPK9_external">Tucson Police Foundation</a></li>
<li><a href="http://www.ajcity.net/police/public_safety.htm" target="PPK9_external">Apache Junction Department of Public Safety</a></li>
<!-- dead link -- <li><a href="http://www.adc.state.az.us/" target="PPK9_external">Arizona Department of Corrections</a></li> -->
<li><a href="http://www.gf.state.az.us/" target="PPK9_external">Arizona Department of Game and Fish</a></li>
<li><a href="http://www.dps.state.az.us" target="PPK9_external">Arizona Department of Public Safety</a></li>
<li><a href="http://www.azpost.state.az.us/" target="PPK9_external">Arizona Peace Officer Standards and Training Board (POST)</a></li>
<li><a href="http://www.asu.edu/east/admin/dps.htm" target="PPK9_external">Arizona State University East Department of Public Safety</a></li>
<li><a href="http://www.avondale.org/police.shtml" target="PPK9_external">Avondale Police Department</a></li>
<li><a href="http://www.asu.edu/dps/" target="PPK9_external">AZ State University Police Department</a></li>
<li><a href="http://www.bullheadcity.com/police/index.asp" target="PPK9_external">Bullhead City Police Department</a></li>
<li><a href="http://www.ci.casa-grande.az.us/cgpd/cgpd.php" target="PPK9_external">Casa Grande Police Department</a></li>
<li><a href="http://www.chandlerpd.com/" target="PPK9_external">Chandler Police Department</a></li>
<li><a href="http://www.cvpolice.com/" target="PPK9_external">Chino Valley Police Department</a></li>
<li><a href="http://www.clarkdale.az.us/police1.html" target="PPK9_external">Clarkdale Police Department</a></li>
<li><a href="http://www.cottonwoodpd.org/frames/htm" target="PPK9_external">Cottonwood Police Department</a></li>
<li><a href="http://www.cityofelmirage.org/police/" target="PPK9_external">El Mirage Police Department</a></li>
<li><a href="http://www.ci.eloy.az.us/police.htm" target="PPK9_external">Eloy Police Department</a></li>
<li><a href="http://www.ftmcdowell.org/policedept.htm" target="PPK9_external">Fort McDowell Tribal Police Department</a></li>
<li><a href="http://www.fountainhillsguide.com/gov_lawenf.html" target="PPK9_external">Fountain Hills Town Marshal</a></li>
<li><a href="http://www.azpolicejobs.com/gila_river.htm" target="PPK9_external">Gila River Tribal Police Department</a></li>
<li><a href="http://www.ci.gilbert.az.us" target="PPK9_external">Gilbert Police Department</a> (<a href="http://www.ci.gilbert.az.us/police/speck91.html" target="PPK9_external">K-9 Unit</a>)</li>
<li><a href="http://www.ci.glendale.az.us/police" target="PPK9_external">Glendale Police Department</a></li>
<li><a href="http://www.goodyearaz.gov/index.asp?ID=199" target="PPK9_external">Goodyear Police Department</a></li>
<li><a href="http://www.eaznet.com/~gcso" target="PPK9_external">Graham County Sheriffs Office</a></li>
<li><a href="http://www.huachucacity.org/HCPD.htm" target="PPK9_external">Huachuca City Police Department</a></li>
<li><a href="http://www.hualapaipolice.com/" target="PPK9_external">Hualapai Tribal Police Department</a></li>
<li><a href="http://www.jeromepd.org" target="PPK9_external">Jerome Police Department</a></li>
<li><a href="http://www.kingmanpolice.com" target="PPK9_external">Kingman Police Department</a></li>
<li><a href="http://www.marana.com" target="PPK9_external">Marana Police Department</a></li>
<li><a href="http://www.mcso.org/" target="PPK9_external">Maricopa County Sheriffs Office</a></li>
<li><a href="http://www.ci.mesa.az.us/police/" target="PPK9_external">Mesa Police Department</a></li>
<li><a href="http://www.ci.tucson.az.us/mantis/" target="PPK9_external">Metropolitan (Tucson) Area Narcotics Trafficking Interdiction Squad (MANTIS)</a></li>
<li><a href="http://www.co.mohave.az.us/mcso/" target="PPK9_external">Mohave County Sheriff Office</a></li>
<li><a href="http://www.nogalespolice.com/" target="PPK9_external">Nogales Police Department</a></li>
<li><a href="http://www.nau.edu/police" target="PPK9_external">Northern Arizona University Police Department</a></li>
<li><a href="http://www.ovpd.org" target="PPK9_external">Oro Valley Police Department</a></li>
<li><a href="http://www.ci.paradise-valley.az.us/police/" target="PPK9_external">Paradise Valley Police Department</a></li>
<?php // (dead link reported 12.27.2004 by danw@therim.com)  <li><a href="http://www.ci.payson.az.us/law.htm" target="PPK9_external">Payson Police Department</a></li> ?>
<li><a href="http://www.peoriaaz.com/police.htm" target="PPK9_external">Peoria Police Department</a></li>
<li><a href="http://www.ci.phoenix.az.us/POLICE/policidx.html" target="PPK9_external">Phoenix Police Department</a></li>
<li><a href="http://www.pima.edu/dps/" target="PPK9_external">Pima County Community College Department of Public Safety</a></li>
<li><a href="http://www.pimasheriff.org/" target="PPK9_external">Pima County Sheriffs Department</a></li>
<li><a href="http://co.pinal.az.us/Sheriff/" target="PPK9_external">Pinal County Sheriff Department</a></li>
<li><a href="http://police.cityofprescott.net" target="PPK9_external">Prescott Police Department</a></li>
<li><a href="http://www.ci.prescott-valley.az.us/Services/police/" target="PPK9_external">Prescott Valley Police Department</a></li>
<li><a href="http://www.sc.maricopa.edu/safety/" target="PPK9_external">Scottsdale Community College Security Department</a></li>
<li><a href="http://www.ci.scottsdale.az.us/COSWEB/SERVICES/PD/index.htm" target="PPK9_external">Scottsdale Police Department</a></li>
<li><a href="http://www.c2i2.com/~hikik/pd/Default.htm" target="PPK9_external">Sierra Vista Police Department</a></li>
<li><a href="http://www.tucson.com/stpd/" target="PPK9_external">South Tucson Police Department</a></li>
<li><a href="http://www.springerville.com/public_safety-police.htm" target="PPK9_external">Springerville Police Department</a></li>
<li><a href="http://www.pcpages.com/superiorpd/" target="PPK9_external">Superior Police Department</a></li>
<li><a href="http://www.surpriseaz.com/index.asp?NID=22" target="PPK9_external">Surprise Police Department</a></li>
<li><a href="http://www.tempe.gov/police/" target="PPK9_external">Tempe Police Department</a></li>
<li><a href="http://www.cityoftombstone.com/marshal.htm" target="PPK9_external">Tombstone City Marshals Department</a></li>
<li><a href="http://www.ci.tucson.az.us/police" target="PPK9_external">Tucson Police Department</a></li>
<li><a href="http://www.uapd.arizona.edu/" target="PPK9_external">University of Arizona Police Department</a></li>
<li><a href="http://www.ci.wickenburg.az.us/wpd.htm" target="PPK9_external">Wickenburg Police Department</a></li>
<li><a href="http://www.willcoxpolice.com" target="PPK9_external">Willcox Department of Public Safety</a></li>
<li><a href="http://www.co.yuma.az.us/so/index.htm" target="PPK9_external">Yuma County Sheriffs Office</a></li>
<li><a href="http://www.ci.yuma.az.us/police.htm" target="PPK9_external">Yuma Police Department</a></li>
</ul>

<p><b>Animal Activism</b></p>
<ul>
<li><a href="http://www.safeanimals.com/" target="PPK9_external">S.A.F.E. - Saving Animals from Euthanasia</a></li>
<li><a href="http://www.peta.org/" target="PPK9_external">P.E.T.A. - People for the Ethical Treatment of Animals</a></li>
<li><a href="http://www.ahscares.org/" target="PPK9_external">Associated Humane Societies of New Jersey</a></li>
</ul>

<p><b>General Pets Websites</b></p>
<ul>
<!-- dead link -- <li><a href="http://www.thepetscorner.com" target="PPK9_external">The Pets Corner</a></li> -->
<li><a href="http://www.maxhasthefacts.com" target="PPK9_external">Max Has the Facts</a></li>
<li><a href="http://www.pantanoac.com/" target="PPK9_external">Pantano Animal Clinic</a></li>
<li><a href="http://www.riggsroadvet.vetsuite.com/Templates/GridCritters.aspx" target="PPK9_external">Riggs Road Vet</a></li>
<li><a href="http://dogsquadu.com" target="PPK9_external">Dog Squad U</a></li>
<li><a href="http://bluebuffalo.com/library/k9-to-5-pawsome-guide-to-working-dogs-infographic/">K9 to 5: A Pawsome Guide to Working Dogs</a></li>
</ul>

<p>
Please report broken links to <a href="javascript:noSpam('ppk9','x.michaelrog.com')" title="Email the webmaster">the webmaster</a>.
</p>

</div>

<?php

include 'includes/footer.php' ;

?>
