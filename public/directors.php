<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>


<div class="maindiv">

<h1>Board of Directors</h1>

<p>
Collectively, the Board of Directors is responsible for the planning, organization, and management of every facet of Protect Police K-9.  They are dedicated to the vision and mission of Protect Police K-9, which would not function without their dedication and support.
</p>

<hr />

<div class="block">

<img src="images/directors/michael-valdez-college.gif" class="bod" alt="Michael Valdez, President and Founder of Protect Police K-9" />

<p>

<b>Michael Valdez, President</b>, founded Protect Police K-9 as a third grade student in 1999, after hearing about an Arizona police dog, Dax, who was wounded and killed in the line of duty. Michael's goal is to provide stab- and bullet-proof vests for all of Arizona's police dogs, and to raise public awareness of the service these dogs provide.

</p><p>

Michael attends Northern Arizona University, in Flagstaff, Arizona. He is currently pursing a degree and certification in Athletic Training and Sports Medicine. He continues to work diligently in school with hopes of continuing his education and passion in the Sports Medicine Profession.  In June of 2004, Michael underwent brain surgery to remove a benign tumor from his brain. Thanks to the skill of his doctors and the support of his family and all the friends of Protect Police K-9, the operation was a great success, and Michael made a speedy and complete recovery.

</p>

</div>

<hr />
    
<div class="block">

<img src="images/directors/gil-valdez.gif" class="bod" alt="Gil Valdez, Vice President of Protect Police K-9" />
<p>

<b>Gil Valdez, Vice President</b>, is a former police officer, and a retired professional driver. Gil devotes most of his time to helping his son Michael run Protect Police K-9. 

</p>

</div>

<hr />

<div class="block">

<img src="images/directors/reagen-kulseth.gif" class="bod" alt="Reagan Kulseth, Protect Police K-9 Media Spokesperson" />

<p>

<b>Reagen Kulseth, Media Spokesperson</b>, served as a Prosecutor with the Pima County Attorney's Office for thirteen years and is currently in private practice with <b>Kulseth & Roads</b> of Tucson.  She is also the President and Co-Founder of <a href="http://www.safeanimals.com/" target="PPK9_external">S.A.F.E. (Saving Animals From Euthanasia</a>.  Reagen has been a close friend of the Valdez family for many years, and currently acts as Protect Police K-9's primary media liaison and legal counsel.

</p>

</div>

<!-- <hr />

<div class="block">

<img src="images/directors/owen-keefe.gif" class="bod" alt="Owen Keefe, Board Member, Protect Police K-9" />
<p>

<b>Owen Keefe</b> is a 19-year veteran of the Scottsdale Police Department.  He's handled four dogs in his 17 years in the K-9 Squad, who's varied assignments include patrol, tracking, drug detection, bomb detection, and SWAT units. He is the lead handler and department trainer for Scottsdale PD, where he is responsible for the training of five K-9 teams.  He works as a National Instructor for Adlerhorst International of Riversdale, California, and as a Certifying Official and Treasurer for the National Police Canine Association (NPCA).  He is also Treasurer for the Arizona Law Enforcement Canine Association (ALECA), and coordinates the Annual Desert Dog K-9 Trials in Scottsdale. 
</p>

</div> -->

<hr />

<div class="block">

<img src="images/directors/ken-colombraro.gif" class="bod" alt="Ken Colombraro, Board Member, Protect Police K-9" />
<p>

<b>Ken Colombraro</b> is currently enjoying retired life following a rewarding 20-year career with the Tucson Police Department. He began working with the Service Dog Unit in 1996, and worked for 6 years with his first K-9, Hondo.  When Hondo retired in 2002, Ken partnered for active duty with Cole, a 2.5-year-old German Shepherd; the two completed their basic training in November of 2002.  Cole, who has apprehended multiple suspects and proven his worth as a police dog many times over, remains in service with the Tucson Police; Ken resides with Hondo in New York.

</p>

</div>

<hr />

<div class="block">

<img src="images/directors/michael-lent.gif" class="bod" alt="Michael P. Lent DVM, Board Member, Protect Police K-9" />
<p>

<b>Michael P. Lent, DVM</b> graduated in 1987 from Bowdoin College in Brunswick, Maine with a major in Biochemistry and a minor in Sociology. In 1991 he received his Doctorate of Veterinary Medicine from Purdue University.  He has lived in Tuscon with his wife Stacey (also a veterinarian) and two boys since 1996. He owns Pantano Animal Clinic and currently serves on the Executive Board of the Arizona Veterinary Medical Association. He is a three-time past President of the Southern Arizona Veterinary Medical Association, and also served as a chairperson for the <a href="http://www.act-az.org/" target="PPK9_external">Animal Cruelty Task Force of Southern Arizona (ACTFSA)</a>.  He is a charter member of ACTFSA, and currently sits on its Steering Committee. He received the Arizona Veterinary Medical Association's "Young Veterinarian of the Year Award" in 2001, and was recognized by the Humane Society of Southern Arizona with a "Humane Education Award" for his work with the Animal Cruelty Task Force and for his service in creating the Pet First Aid course taught by the Red Cross of Southern Arizona.

</p>

</div>

<hr />

<div class="block">

<img src="images/directors/angel-leos.gif" class="bod" alt="Lt. Angel Leos, Board Member, Protect Police K-9" />
<p>

<b>Lt. Angel Leos</b> began his career with the Arizona Department of Public Safety in September of 1989. After his completion of the Police Academy, Lt. Leos was assigned to the highway patrol in the Casa Grande Arizona area, where he became proficient in the area of drug interdiction. In 1993 he was assigned his first drug detection canine and subsequently obtained Instructor certifications in Canine Drug Detection, Canine Patrol and Canine Explosives Detection.  Lt. Leos currently serves as the Canine District Commander for the Arizona Department of Public Safety, where his unit is responsible for seizing thousands of pounds of drugs and millions of dollars in illicit currency each year.

</p>

</div>

</div>

<?php

include 'includes/footer.php' ;

?>