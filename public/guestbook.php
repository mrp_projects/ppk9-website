<?php

/*  ArdGuest 1.7
[http://www.promosi-web.com/script/guestbook/]
Copyright (c) 2001 - 2005, Ketut Aryadana [ketutaryadana@yahoo.co.id]  */

  $title = "PPK9 Guestbook";
  $admin_password = "sarasarge10";
  $admin_email = "gil.valdez@protectpolicek-9.com";
  $home = "http://www.protectpolicek-9.com";
  $notify = "YES";
  $os = "UNIX";
  $images_directory = "images/guestbook" ;
  $max_entry_per_page = "400";
  $data_file = "data/guestbook.dat";
  $max_record_in_data_file = "9900";
  $max_entry_per_session = 3;

$do = isset($_REQUEST['do']) ? trim($_REQUEST['do']) : "";
$id = isset($_GET['id']) ? trim($_GET['id']) : "";
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$self = $_SERVER['PHP_SELF'];

if (!file_exists($data_file)) {
    echo "<strong>Error:</strong> Can't find data file <strong>$data_file</strong>";
	exit;
} else {
  if ($max_record_in_data_file != "0") {
     $f = file($data_file);
     rsort($f);
     $j = count($f);
     if ($j > $max_record_in_data_file) {
        $rf = fopen($data_file,"w");
            if (strtoupper($os)=="UNIX") {		    		 
	           if (flock($rf,LOCK_EX)) {
                  for ($i=0; $i<$max_record_in_data_file; $i++) {
                      fwrite($rf,$f[$i]);	     
			      }
                  flock($rf,LOCK_UN);
	           }
            } else {
               for ($i=0; $i<$max_record_in_data_file; $i++) {
                  fwrite($rf,$f[$i]);	     
	           }
	        }
	    fclose($rf);
     }
  }
}
session_start();
$newline = (strtoupper($os) == "WIN") ? "\r\n" : "\n";
switch ($do) {
case "":
   $record = file($data_file);
   rsort($record);
   $jmlrec = count($record);
?>

<?php

include 'includes/header.php' ; include 'includes/masthead.php' ; include 'includes/navdiv.php' ; ?>

<!-- guestbook script powered by:  Ardguest 1.7
(see http://www.promosi-web.com/script/guestbook/ for more info) -->

<div class="maindiv">

<h1><?=$title?></h1>
<p>Please <strong><a href="<?="$self?do=add_form&page=$page"?>">leave a message</a></strong> of encouragement or support for the PPK9 team in our guestbook.<p>
<p><em>Questions about PPK9 events or operations should be addressed directly to a team member (not in the guestbook) so that we can respond to your inquiry promptly. See the <a href="contact.php">contact page</a> for specific options.</em></p>
<hr />

<?
      $jml_page = intval($jmlrec/$max_entry_per_page);
      $sisa = $jmlrec%$max_entry_per_page;
      if ($sisa > 0) $jml_page++;
      $no = $page*$max_entry_per_page-$max_entry_per_page;
      if ($jmlrec == 0) echo "<p>There are no entries yet.</p>";

		$w = 0; //--Color
        for ($i=0; $i<$max_entry_per_page; $i++) {
		    $no++;
		    $recno = $no-1;
		    if (isset($record[$recno])) {
		       $row = explode("|~|",$record[$recno]);
			   if ($w==0) { 
				   $warna = $table_content_1a;
				   $warna2 = $table_content_1b;
				   $w=1;
			   } else { 
				   $warna = $table_content_2a;
				   $warna2 = $table_content_2b;
				   $w=0;
			   }

			   // if a homepage exists, put the name in a link... otherwise, just print the name
			   if (trim($row[6]) != "" && trim($row[6]) != "http://") {
				   if (ereg("^http://", trim($row[6]))) echo "<p>...from <strong><a href=\"$row[6]\" target=\"PPK9_external\">$row[3]</a></strong>";
				   else echo "<p>...from <strong><a href=\"http://$row[6]\" target=\"PPK9_external\">$row[3]</a></strong>";
				   } else { echo "<p>...from <strong>$row[3]</strong>"; }
			   
						
				// check city and state values and print those which are not empty in proper format
			   			if (trim($row[8]) != "" && trim($row[9]) != "" ) {   
							echo " ($row[8], $row[9])";
						} elseif (trim($row[8]) != "" && trim($row[9]) == "" ) {
							echo " ($row[8])";
						} elseif (trim($row[8]) == "" && trim($row[9]) != "" ) {
							echo " ($row[9])"; 
						}
						
				echo " on $row[2]:";			   
				echo "<blockquote>".stripslashes($row[5])."</blockquote>" ;
			   
// ## ADMIN OPTIONS
/*
echo "<blockquote>" ;
if (trim($row[4]) != "") {
	$email_parts = explode("@",$row[4]);
	echo "<a href=\"mailto:javascript:noSpam('$email_parts[0]','$email_parts[1]')\"><img src=\"$images_directory/email.gif\" alt=\"email $row[3]\"></a>" ;
}
if (trim($row[6]) != "" && trim($row[6]) != "http://") {
	if (ereg("^http://", trim($row[6]))) echo " <a href=\"$row[6]\" target=\"PPK9_external\"><img src=\"$images_directory/homepage.gif\" alt=\"$row[6]\"></a>";
	else echo " <a href=\"http://$row[6]\" target=\"PPK9_external\"><img src=\"$images_directory/homepage.gif\" alt=\"$row[6]\"></a>";
	}
echo " <a href=\"$self?do=del&id=$row[1]&page=$page\"><img src=\"$images_directory/del.gif\" alt=\"delete entry # $no\"></a>" ;
echo "</blockquote>" ;
*/
// ## end ADMIN OPTIONS	
		
	echo "</p>" ;
					 
			} //--end if		
        } //--end for

 /*  PAGE NUMBER BLOCK
 		if ($jml_page > 1) {	   
		  if ($page != 1) echo "[<a href=\"$self?page=1\">Top</a>] "; else echo "[Top] "; 
	      echo "Page # ";
          if ($jml_page > 10) {
	 	      if ($page < 5) {
		          $start = 1;
			      $stop = 10;
		      } elseif ($jml_page - $page < 5) {
		          $start = $jml_page - 9;
			      $stop = $jml_page;
		      } else {
		          $start = $page-4;
			      $stop = $page+5;
			  }
		      if ($start != 1) echo "... ";
              for ($p=$start; $p<=$stop; $p++) {
				  if ($p == $page) echo "<b>$p</b>";
				  else echo "<a href=\"$self?page=$p\">$p</a>";
              }
		      if ($stop != $jml_page) echo "... ";		 		 
		      echo "of $jml_page ";
          } else {
              for ($p=1; $p<=$jml_page; $p++) {
	              if ($p == $page) echo "<b>$p</b>";
			      else echo "<a href=\"$self?page=$p\">$p</a>";
              }
	      }	   
          if ($page != $jml_page) echo "[<a href=\"$self?page=$jml_page\">Bottom</a>]";
		  else echo "[bottom]"; 
      }
*/

?>

<hr />
<p>Please <a href="<?="$self?do=add_form&page=$page"?>">leave us a message</a>!</p>
</div>
<?php include 'includes/footer.php' ; ?>
   
   
<?
break;
case "add_form":
$_SESSION['secc'] = strtoupper(substr(md5(time()),0,4));
if (!isset($_SESSION['add'])) $_SESSION['add'] = 0;

if (!isset($_SESSION['name'])) $_SESSION['name'] = "";
if (!isset($_SESSION['email'])) $_SESSION['email'] = "";
if (!isset($_SESSION['url'])) $_SESSION['url'] = "http://";
if (!isset($_SESSION['comment'])) $_SESSION['comment'] = "";
?>

<?php include 'includes/header.php' ; include 'includes/masthead.php' ; include 'includes/navdiv.php' ; ?>
<div class="maindiv">

<h1><?=$title?></h1>
<p><a href="<?=$self?>">back to entries</a>
<p>Please leave a message of encouragement or support for the PPK9 team in our guestbook.  Your <strong>name</strong> and <strong>message</strong> are <em><strong>required</strong></em>.  You may leave an e-mail address, website URL, and your place of residence, but all these are optional. <em>Your e-mail address will not be made public or shared with any third party.</em></p>
<p>Please direct all <em>questions</em> about PPK9 events or operations to the appropriate team member directly (not in the guestbook) so that we can respond to your inquiry promptly. (See the <a href="contact.php">contact page</a> for specific options.)</p>

<form method="post" action="<?=$self?>">
<label for="vname"><strong><em>name</em></strong></label><input type="text" name="vname" id="vname" size="30" maxlength="70" value="<?=$_SESSION['name']?>"><br />
<label for="vcity">city</label><input type="text" name="vcity" id="vcity" size="30" maxlength="150" value="<?=$_SESSION['city']?>"><br />
<label for="vstate">state</label><input type="text" name="vstate" id="vstate" size="30" maxlength="150" value="<?=$_SESSION['state']?>"><br />
<label for="vemail">e-mail</label><input type="text" name="vemail" id="vemail" size="30" maxlength="100" value="<?=$_SESSION['email']?>"><br />
<label for="vurl">website</label><input type="text" name="vurl" id="vurl" size="30" maxlength="150" value="<?=$_SESSION['url']?>"><br />
<label for="vcomment"><strong><em>message</em></strong></label><textarea name="vcomment" id="vcomment" cols="40" rows="7" wrap="virtual"><?=$_SESSION['comment']?></textarea><br />
<label for="vsecc">verification code</em></strong></label><input type="text" name="vsecc" id="vsecc" size="4" maxlength="4"> <b><?=$_SESSION['secc']?></b> (retype this code)<br />
<input type="hidden" name="do" value="add"><br />
<input type="submit" value="Submit this Message">
</form>

</div>
<?php include 'includes/footer.php' ; ?> 

<?
break;
case "add":
   $vname = isset($_POST['vname']) ? trim($_POST['vname']) : "";
   $vemail = isset($_POST['vemail']) ? trim($_POST['vemail']) : "";
   $vurl = isset($_POST['vurl']) ? trim($_POST['vurl']) : "";
   $vcomment = isset($_POST['vcomment']) ? trim($_POST['vcomment']) : "";
   $vsecc = isset($_POST['vsecc']) ? strtoupper($_POST['vsecc']) : "";

   if (strlen($vname) > 70) $vname = substr($vname,0,70);
   if (strlen($vemail) > 100) $vemail = substr($vemail,0,100);
   if (strlen($vurl) > 150) $vurl = substr($vurl,0,150);

   $_SESSION['name'] = $vname;
   $_SESSION['email'] = $vemail;
   $_SESSION['url'] = $vurl;
   $_SESSION['city'] = $vcity;
   $_SESSION['state'] = $vstate;
   $_SESSION['comment'] = stripslashes($vcomment);

   if ($vname == "" || $vcomment == "") {
	   input_err("You may left some fields.");
   }

   if ($vemail != "" && !preg_match("/([\w\.\-]+)(\@[\w\.\-]+)(\.[a-z]{2,4})+/i", $vemail)) {
	   input_err("Invalid email address.");
   }

   if ($vurl != "" && strtolower($vurl) != "http://") {
       if (!preg_match ("#^http://[_a-z0-9-]+\\.[_a-z0-9-]+#i", $vurl)) {
		   input_err("Invalid URL format.");
       }
   }

   $test_comment = preg_split("/[\s]+/",$vcomment);
   $jmltest = count($test_comment);
   for ($t=0; $t<$jmltest; $t++) {
      if (strlen(trim($test_comment[$t])) > 70) {
		  input_err("Invalid word found on your entry : ".stripslashes($test_comment[$t]));
	  }
   }

   if (isset($_SESSION['add']) && $_SESSION['add'] >= $max_entry_per_session) {
	   input_err("Sorry, only $max_entry_per_session message(s) allowed per session.",false);
   } elseif (!isset($_SESSION['add'])) {
	   exit;
   }

   if ($vsecc != $_SESSION['secc']) {
	   input_err("Invalid verification code");
   }
   //--only 2000 characters allowed for comment, change this value if necessary
   $maxchar = 2000;
   if (strlen($vcomment) > $maxchar) $vcomment = substr($vcomment,0,$maxchar)."...";

   $idx = date("YmdHis");
   $tgl = date("F d, Y");

   $vname = str_replace("<","&lt;",$vname);
   $vname = str_replace(">","&gt;",$vname);
   $vname = str_replace("~","-",$vname);
   $vname = str_replace("\"","&quot;",$vname);
   $vcomment = str_replace("<","&lt;",$vcomment);
   $vcomment = str_replace(">","&gt;",$vcomment);
   $vcomment = str_replace("|","",$vcomment);
   $vcomment = str_replace("\"","&quot;",$vcomment);
   $vurl = str_replace("<","",$vurl);
   $vurl = str_replace(">","",$vurl);
   $vurl = str_replace("|","",$vurl);
   $vemail = str_replace("<","",$vemail);
   $vemail = str_replace(">","",$vemail);
   $vemail = str_replace("|","",$vemail);
   $vcity = str_replace("<","",$vcity);
   $vcity = str_replace(">","",$vcity);
   $vcity = str_replace("|","",$vcity);
   $vstate = str_replace("<","",$vstate);
   $vstate = str_replace(">","",$vstate);
   $vstate = str_replace("|","",$vstate);

   if (strtoupper($os) == "WIN") {
	   $vcomment = str_replace($newline,"<br>",$vcomment);
	   $vcomment = str_replace("\r","",$vcomment);
	   $vcomment = str_replace("\n","",$vcomment);
   } else {
	   $vcomment = str_replace($newline,"<br>",$vcomment);
	   $vcomment = str_replace("\r","",$vcomment);
   }

   if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && eregi("^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$",$_SERVER['HTTP_X_FORWARDED_FOR'])) {
       $ipnum = $_SERVER['HTTP_X_FORWARDED_FOR'];
   } else {
       $ipnum = getenv("REMOTE_ADDR");
   }

   $newdata = "|~|$idx|~|$tgl|~|$vname|~|$vemail|~|$vcomment|~|$vurl|~|$ipnum|~|$vcity|~|$vstate|~|";
   $newdata = stripslashes($newdata);
   $newdata .= $newline;

   $tambah = fopen($data_file,"a");
   if (strtoupper($os)=="UNIX") {
       if (flock($tambah,LOCK_EX)) {
	       fwrite($tambah,$newdata);
	       flock($tambah,LOCK_UN);
       }
   } else {
	   fwrite($tambah,$newdata);
   }
   fclose($tambah);

   if (strtoupper($notify) == "YES") {
       $msgtitle = "There is a new entry in the PPK9 guestbook.";
       $vcomment = str_replace("&quot;","\"",$vcomment);   
       $vcomment = stripslashes($vcomment);
	   $vcomment = str_replace("<br>","\n",$vcomment);
       $msgcontent = "The addition from $vname :\n\n----------------------------\n\n$vcomment\n\n-----End Message-----";
       $msgcontent .= "\n\n(see http://www.protectpolicek-9.com/guestbook.php)" ;
       @mail($admin_email,$msgtitle,$msgcontent,"From: $vemail\n");
   }
   //--clear session
   $_SESSION['name'] = "";
   $_SESSION['email'] = "";
   $_SESSION['city'] = "";
   $_SESSION['state'] = "";
   $_SESSION['url'] = "http://";
   $_SESSION['comment'] = "";
   $_SESSION['add']++;
   $_SESSION['secc'] = "";
   redir($self,"Thank you, your entry has been added.");
break;

case "del":
   $record = file($data_file);
   $jmlrec = count($record);
   for ($i=0; $i<$jmlrec; $i++) {
       $row = explode("|~|",$record[$i]);
	   if ($id == $row[1]) {
	      ?>
<?php

include 'includes/header.php' ; include 'includes/masthead.php' ; include 'includes/navdiv.php' ; ?>

<div class="maindiv">
<h3>Guestbook: delete confirmation</h3>

<p>message from <b><?=$row[3]?></b> (<?=$row[2]?>)
<blockquote><?=$row[5]?></blockquote>
<blockquote>IP : <?=$row[7]?></blockquote>
</p>
			
<form action="<?=$self?>" method="post">
<input type="hidden" name="do" value="del2">
<input type="hidden" name="id" value="<?=$id?>">
<input type="hidden" name="page" value="<?=$page?>"><br />
<label for="pwd">admin. password:</label><input type="password" name="pwd" id="pwd"><br />
<label for="byip">expunge IP</label><input type="checkbox" name="byip" id="byip" value="<?=$row[7]?>"> (delete all messages from IP <?=$row[7]?>)<br />
<input type="submit" value="Delete">
</form>
		  
</div>
<?php include 'includes/footer.php' ; ?>
			  
		  <?
	   }
   }      
break;

case "del2":
   $pwd = isset($_POST['pwd']) ? trim($_POST['pwd']) : "";
   $id = isset($_POST['id']) ? trim($_POST['id']) : "";
   $page = isset($_POST['page']) ? $_POST['page'] : 1;
   $byip = isset($_POST['byip']) ? $_POST['byip'] : "";

   if ($pwd != $admin_password) {
	     redir("$self?page=$page","Invalid admin password !");
   }

   $record = file($data_file);
   $jmlrec = count($record);
   for ($i=0; $i<$jmlrec; $i++) {
       $row = explode("|~|",$record[$i]);
	   if ($byip == "") {
		   if ($row[1] == $id) {
			   $record[$i] = "";
		       break;
	       }
	   } else {
		   if ($row[7] == $byip) {
			   $record[$i] = "";
		   }
	   }
   }

   $update_data = fopen($data_file,"w");
   if (strtoupper($os) == "UNIX") {
      if (flock($update_data,LOCK_EX)) {
	     for ($j=0; $j<$jmlrec; $j++) {
             if ($record[$j] != "") {
				 fputs($update_data,$record[$j]);
			 }
		 }
		 flock($update_data,LOCK_UN);
	  }
   } else {
	     for ($j=0; $j<$jmlrec; $j++) {
             if ($record[$j] != "") {
				 fputs($update_data,$record[$j]);
			 }
		 }
   }
   fclose($update_data);
   redir("$self?page=$page","Record has been deleted !");
break;
} //--end switch


function redir($target,$msg) {

?>
<html>
<head><title><?=$msg?></title><meta http-equiv="refresh" content="1; url=<?=$target?>"></head>
<body>
<h1><?=$msg?></h1><h3>Please wait...</h3>
</body>
<html>
<?
exit;
}

function input_err($err_msg,$linkback=true) {
global $title;
?>

<?php include 'includes/header.php' ; include 'includes/masthead.php' ; include 'includes/navdiv.php' ; ?>
<div class="maindiv">
<h1><?=$title?></h1>

<p><strong><?=$err_msg?></strong>
<?if ($linkback) {?> Click <a href="javascript:history.back()">here</a> and try again.<?}?>
</div>
<?php include 'includes/footer.php' ; ?>
	    
<?
exit;
}
?>