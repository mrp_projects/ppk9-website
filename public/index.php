<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>

<div class="maindiv">

<!-- <div class="urgent">
<p><b>BREAKING NEWS:</b></p>
<p>Protect Police K9 is currently working with Arizona legislators to introduce House Bill 2120, which proposes stiffer laws regarding the protection of working police dogs and and provides state funding for K9 vests.  More information is available on the <a href="HB-2120.php">HB-2120 information page</a>.</p>
</div><hr> -->

<!--

<h1>Resignation of President Michael Valdez</h1>

<p>Dear friends -</p>

<p>On June 1<sup>st</sup>, 2009, I will step down from my role as President of <em>Arizona Protect Police K-9</em> in order to pursue my college education. The 10+ years I have invested in this wonderful program have been immensely rewarding to me, and although our mission is still not complete, I take great pride in knowing that our slogan - "Protecting the dogs who protect us" - has truly been lived out through our efforts. The lessons I have learned along this journey have shaped me as a person, and the many friends I have made continue to provide great support.</p>

<p>Since our founding, we have raised more than $100,000 and vested more than 160 law enforcement service dogs. This is an extraordinary accomplishment, and I am be humbled by the dedication and faithfulness of PPK9's Directors and supporters. I extend my deepest thanks to you and to all those who have been a part of the PPK9 mission in one way or another throughout my tenure: All our accomplishments are products of your support!</p>

<p>Of course, as I have said, our mission is still not quite complete. The important work of vesting our law enforcement service dogs must continue, and new leaders must step up to continue the momentum we have established. <strong>Effective immediately, we are seeking candidates interested in taking over this organization.</strong> The next President (and Vice-President) of PPK9 will have the full support and assistance of the Board of Directors. Our current Vice-President, Mr. Gil Valdez, will continue to serve in his present role until a suitable replacement is installed, and he will remain active as an advisor as required to ensure a smooth and successful transition to new leadership.</p>

<p><strong>Please <a href="mailto:gil@protectpolicek-9.com">contact Gil Valdez</a> for more information or to submit a resume.</strong></p>

<p>Once again, my deepest thanks to everyone who has been a part of this organization...</p>

<p>With great respect,</p>

<p><strong>Michael Valdez</strong><br />President, Arizona Protect Police K-9</p>

<p>&nbsp;</p>

-->

<!--

<h1>PPK9 in the news!</h1>
<dl class="news">
<dt>04.03.2009 - <a href="http://www.zwire.com/site/news.cfm?newsid=20292197" target="PPK9_external">Group provides vests to police dogs</a></dt>
<dt>12.14.2006 - <a href="http://www.azstarnet.com/sn/relatedstories/160373.php" target="PPK9_external">Names of DPS dogs now honor memory of fallen officers</a></li>
<dt>09.22.2006 - <a href="http://www.azstarnet.com/metro/147841" target="PPK9_external">Fans of Fallen Tucson Police K-9 Miko raise $10,000 for New Police K-9</a></li>
</dl>


<p>&nbsp;</p>

-->

<h1>The PPK9 mission: <em>protecting the dogs who faithfully protect us</em></h1>

<p>
Dear friends -
</p>

<p align="justify">
A very special and elite team exists of law-enforcement officers who, having undergone years of rigorous training, prove daily their loyalty and dedicatition to their community.  Whether you've heard of them or not, they work day in and day out - ready to risk their lives at a moment's notice - to protect and serve you.  The fundamental difference between these and any other law enforcement team, however, isn't their speed, agility, intelligence, bravery, or commitment:  it's their legs - all four of them.</p>

<p align="justify">
Almost everyone has heard that a dog is Man's best friend.  For almost three-hundred Arizona police dogs, though, such a statement has much more significance than you may realize.  They search and rescue, they sniff out dangerous drugs and explosives, they track criminals, detain dangerous persons, and occasionally even make traffic stops.  They work hard to faithfully protect you.
</p>

<p align="justify">
I started Protect Police K-9 a few years ago when I heard about a Tucson Police K-9, Dax, who was shot by a suspect during a chase.  I believe that all our law officers, especially the four-legged ones, deserve proper protection as they work to faithfully protect us.  In this spirit, I set out to see that all of Arizona's police dogs are equipped with stab- and bullet-proof vests.  In only a few years, with the help of many generous and caring people, we've seen the realization of this goal in all of Southern Arizona's police dogs.  However, about two-hundred dogs in Northern Arizona's law enforment agencies still need vests.
</p>

<p align="justify">
Each protective vest costs over $825, which is out of reach for the limited budgets of many agencies.  For this reason, I need YOUR support to make our dream a complete reality.  I need YOU to help us vest Arizona's police dogs.  Please explore our website to find out about PPK9's mission and how you can help.  To invest in the safety and protection of a Police K-9 is to invest in the safety and protection of your community - a truly worthwhile cause.
</p>

<p align="justify">
Thanks for your support,
</p>

<!-- <img src="images/signatures/michaelvaldez.gif" alt="Michael Valdez's signature" /> -->

<p>
Michael Valdez </p>
<p align="justify">&nbsp; </p>

<p><strong style="color: red;">Please note: For legal reasons, PPK9 cannot assist the public with finding homes for dogs and cannot provide contact information to anyone attempting to place an animal with a law enforcement agency. We apologize for the inconvenience.</strong></p>

<p align="justify">  <i>Protect Police K-9 is a 501(c)3 non-profit organization: EIN 20-0168077.</i>
</p>
</div>

<?php

include 'includes/footer.php' ;

?>