<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>


<div class="maindiv">

<h1>How to Contact Protect Police K-9</h1>

<p><b>Mailing Address:</b></p>
<p>Protect Police K-9<br />2558 E. Eastland St.<br />Tucson, Arizona 85716-5721</p>

<p><b>Phone Numbers:</b></p>
<p>HQ: (520) 326-4904<br />Fax: <em>Call HQ for fax number</em><br />Cell: (520) 241-1235</p>

<p><strong class="red">Please note: For legal reasons, PPK9 cannot assist the public with finding homes for dogs and cannot provide contact information to anyone attempting to place an animal with a law enforcement agency. If you are looking for a home for an animal, we encourage you to contact the <a class="red" href="http://pimaanimalcare.org">Pima Animal Care Center</a>, the <a class="red" href="http://www.hssaz.org">Humane Society of Southern Arizona</a>, or <a class="red" href="http://www.safeanimals.com">S.A.F.E. Animal Rescue</a> for additional advice.</strong></p>

<p>For general information about Protect Police K-9, contact<br />
<b>Michael Valdez - <a title="Michael.Valdez [at] ProtectPoliceK-9.com" href="mailto:michael.valdez@protectpoliceK-9.com">Michael.Valdez [at] ProtectPoliceK-9.com</a></b></p>

<p>To <strong>leave a message</strong> of encouragement or support for the PPK9 team, please visit the <a href="guestbook.php">PPK9 Guestbook</a>.</p>

<p>To <strong>request a vest</strong> for your K-9 or Unit, visit the <a href="request.php">request page</a>.</p>

<p>For information about Public Relations opportunities or Fundraising events, contact<br />
<b>Lynsey Phillips - <a title="Lynsey.Phillips [at] ProtectPoliceK-9.com" href="mailto:lynsey.phillips@protectpolicek-9.com">Lynsey.Phillips [at] ProtectPoliceK-9.com</a></b></p>

<?php /* <p>For information about Police dogs and their role in Law Enforcement, contact<br />
<b>Officer Ken Colombraro - <a title="Ken.Colombraro [at] ProtectPoliceK-9.com" href="mailto:Ken.Colombraro@ProtectPoliceK-9.com<">Ken.Colombraro [at] ProtectPoliceK-9.com</a></b></p> */ ?>

<?php /* <p>For information about Animal Cruelty Laws, contact<br /><b>Detective Mike Duffy - <a href="mailto:detectivemikeduffy@protectpolicek-9.com">DetectiveMikeDuffy@ProtectPoliceK-9.com</a></b></p> */ ?>

<?php /* <p>For information regarding Mesa/Phoenix K-9 Units, contact<br /><b>Officer Gerald Allen - <a title="Gerry.Allen [at] ProtectPoliceK-9.com" href="javascript:noSpam('gerry.allen','protectpoliceK-9.com')">Gerry.Allen [at] ProtectPoliceK-9.com</a></b></p> */ ?>

<!-- <p>For information regarding Metropolitan/Scottsdale K-9 units, Aleca, or NPCA, contact<br />
<b>Owen Keefe - <a title="Owen.Keefe [at] ProtectPoliceK-9.com" href="javascript:noSpam('owen.keefe','protectpoliceK-9.com')">Owen.Keefe [at] ProtectPoliceK-9.com</a></b></p>
-->
 
<?php /* <p>For information about the Arizona Deptartment of Public Safety K9 Unit and their role in Arizona law enforcement, contact<br />
<b>Lt. J. A. Leos - <a title="Angel.Leos [at] ProtectPoliceK-9.com" href="mailto:angel.leos@protectpolicek-9.com">Angel.Leos [at] ProtectPoliceK-9.com</a></b></p> */ ?>

<?php /* <p>For legal matters or information regarding grant writing, contact<br />
<b>Reagen Kulseth - <a title="Reagen.Kulseth [at] ProtectPoliceK-9.com" href="mailto:reagen.kulseth@protectpoliceK-9.com">Reagen.Kulseth [at] ProtectPoliceK-9.com</a></b></p> */ ?>

<?php /* <p>For legal matters or information regarding veterinary medicine or animal rights (ACTFSA), contact<br />
<b>Dr. Michael Lent - <a title="Michael.Lent [at] ProtectPoliceK-9.com" href="mailto:michael.lent@protectpoliceK-9.com">Michael.Lent [at] ProtectPoliceK-9.com</a></b></p> */ ?>

<!--<p>For more information regarding PPK9 involvement in the creation of <a href="HB-2120.php">Arizona House Bill #2120</a>, or to find out how you can support our efforts to improve animal laws, contact<br />
<b>Jo Grant - <a title="jogrant [at] azleg.state.az.us" href="javascript:noSpam('jogrant','azleg.state.az.us')">jogrant [at] azleg.state.az.us</a></b></p>
-->

<p>For questions or comments about our website, contact our webmaster<br />
<b>Michael Rog <a title="Michael.Rog [at] ProtectPoliceK-9.com" href="michael.rog@protectpoliceK-9.com">Michael.Rog [at] ProtectPoliceK-9.com</a> | <a href="http://michaelrog.com">http://michaelrog.com</a></b></p>

</div>

<?php

include 'includes/footer.php' ;

?>