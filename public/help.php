<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>


<div class="maindiv">

<h1>You can help protect our Police dogs.</h1>

<p>
The mission of Protect Police K-9 is to assure that Arizona's police dogs, who are routinely placed in life-threatening situations, are as well-protected as their human partners.  Given the substantial investment in each dog's training and care, it makes sense to protect them as they serve our community.  However, many law enforcement organizations cannot afford the expense of vest protection for their K9 units.  Due to the expense in manufacturing high-quality ballistic materials, each vest costs about $825.
</p>

<div class="urgent">
<p><b>URGENT NEED:</b></p>
<p>Due to an increased demand for body armor for U.S. military personnel stationed internationally, the cost of K-9 vests has increased significantly. Protect Police K-9 is urgently seeking support to offset these cost increases. Please read the information below and consider helping as you are able.</p>
</div>

<ul>
<li>
<p><b>1. Donate Funds to Protect Police K9</b></p>
<p>
Donations of any amount are gratefully accepted and desperately needed.  Protect Police K-9 is a 501(c)3 non-profit organization, so your donations are tax-deductible.</p>

	<ul>

	<li>
	<p><b>Donate by Mail</b></p>
	<p>Protect Police K-9<br />2558 E. Eastland St.<br />Tucson, AZ 85716-5721</p>
	</li>

	<li>
	<p><b>Donate by Direct Deposit</b></p>
	<p>BankOne / JPMorgan Chase Bank<br />Account # 0711-9055<br />(3033 E. Broadway Blvd.)<br />Tucson, AZ 85716</p>
	<p>(If you donate by direct deposit, please <a href="mailto:gil@protectpolicek-9.com">send us a note</a> detailing the time and donation of your deposit so we can send you a tax-deduction credit once we receive confirmation.)</p>
	</li>

	<li>
	<p><b>Donate through PayPal</b></p>
	<p>Protect Police K-9 is PayPal certified.<br /><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=Protectpolicek%2d9%40cox%2enet&no_shipping=0&no_note=1&tax=0&currency_code=USD&charset=UTF%2d8&charset=UTF%2d8"><img src="images/paypal/donate-text.gif" /></a></p>
	</li>
	</ul>
</li>

<li>
<p><b>2. Sponsor a Vest or Police K9 Unit</b></p>
<p>Protect Police K-9 welcomes corporate donations and sponsorships of vests and units.  For more information about sponsorships and public relations opportunities, please contact <a href="mailto:gil@protectpolicek-9.com">Gil Valdez</a>.</p>
</li>

<li>
<p><b>3. Support Fundraising Efforts</b></p>
<p>The success of Protect Police K-9's fundraising efforts is dependent upon folks like you sharing our mission with others.  Please, encourage your friends and family to support Protect Police K-9 through their gifts of time or donations.  We welcome the efforts of individuals, service organizations, and corporations to spread the word about Protect Police K-9 by volunteering time to help with our fundraising efforts.  If you wish to volunteer your time at a <a href="events.php">Protect Police K-9 event</a>, please contact <a href="mailto:gil@protectpolicek-9.com">Gil Valdez</a> for more information.</p>
</li>

<li>
<p><b>4. Serve as a Grant Writer</b></p>
<p>PPK9 is in need of dedicated <b>grant writers</b> to donate their time and services - please contact  <a href="mailto:reagen@protectpolicek-9.com">Reagen Kulseth</a> if you are willing to help us.
</li>

<li>
<p><b>5. Protect Police K-9s where You Live</b></p>
<p>The need to protect our police dogs doesn't stop at Arizona's border.  You can aid the cause of protecting police canines in your area.  Consider the tips below, and check our <a href="links.php">links page</a> to see if there is a Vest-a-Dog organization in your area.</p>

	<ul>
	<li>
	<p><b>Contact Local and State Police</b></p>
	<p>How many canine officers are on duty in your area and if they have protective gear.</p>
	</li>

	<li>
	<p><b>Organize a Fundraiser</b></p>
	<p>You can sponsor a dog in your town, or team together with other groups and sponsor an entire K-9 unit. Fundraising makes a great project for schools, Boy Scout and Girl Scout groups, church groups, etc.  PPK9's founder, <a href="mailto:michael@protectpolicek-9.com">Michael Valdez</a>, can help get you started with some fundraising ideas.</p>
	</li>

	<li>
	<p><b>Contact K9 Organizations</b></p>
	<p>Determine if there is an organization in your state that is working to pass laws and raise funds to provide gear for these animals and find out how you can contribute to their mission. Police dog associations, animal protection organizations and breed rescue groups might be able to help you with this information.</p>
	</li>

	<li>
	<p><b>Contact your Government</b></p>
	<p>Write letters to your local, state and federal representatives, calling on them to pass legislation that would mandate funding for protective gear for all police working dogs.</p>
	</li>

	<li>
	<p><b>Start a local Vest-a-Dog Group</b></p>
	<p>Visit the <a href="http://www.dogvest.com" target="_blank">National Vest-A-Dog organization (www.dogvest.com)</a> for additional information about how you can start your own dog vesting group to protect police K-9s.</p>
	</li>
	</ul>
</li>

</ul>

<p>
Your help is deeply appreciated.  Only through your support are we able to protect our Police K-9s.
</p>

</div>

<?php

include 'includes/footer.php' ;

?>