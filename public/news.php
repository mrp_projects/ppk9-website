<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>


<div class="maindiv">

<h1>PPK9: In the News</h1>

<h3>Protect Police K-9 featured on Flagstaff 12 News</h3>

<div>
<object id="flashObj" width="486" height="412" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,47,0"><param name="movie" value="http://c.brightcove.com/services/viewer/federated_f9?isVid=1" /><param name="bgcolor" value="#FFFFFF" /><param name="flashVars" value="omnitureAccountID=gpaper158&pageContentCategory=videonetwork&pageContentSubcategory=videonetwork&marketName=Phoenix&revSciSeg=D08734_70008|D08734_70107|D08734_70748|D08734_72008|D08734_72020|D08734_72021|D08734_72772|D08734_72773|D08734_70012|D08734_70052|D08734_70057|D08734_70620|D08734_70086|D08734_70087|D08734_70009|D08734_70010|D08734_70038|D08734_70040|D08734_70066|D08734_70074|J06575_10557|J06575_10701|J06575_50735|J06575_50778|J06575_50892&revSciZip=null&revSciAge=null&revSciGender=null&division=Newspaper&SSTSCode=gci-az-phoenix.com/video/news_Video_prestream&videoId=768106922001&playerID=49625183001&playerKey=AQ~~,AAAABvZFMzE~,IXjx0MpOF0pugpuviAwD9l3_WMhvmNP7&domain=embed&dynamicStreaming=true" /><param name="base" value="http://admin.brightcove.com" /><param name="seamlesstabbing" value="false" /><param name="allowFullScreen" value="true" /><param name="swLiveConnect" value="true" /><param name="allowScriptAccess" value="always" /><embed src="http://c.brightcove.com/services/viewer/federated_f9?isVid=1" bgcolor="#FFFFFF" flashVars="omnitureAccountID=gpaper158&pageContentCategory=videonetwork&pageContentSubcategory=videonetwork&marketName=Phoenix&revSciSeg=D08734_70008|D08734_70107|D08734_70748|D08734_72008|D08734_72020|D08734_72021|D08734_72772|D08734_72773|D08734_70012|D08734_70052|D08734_70057|D08734_70620|D08734_70086|D08734_70087|D08734_70009|D08734_70010|D08734_70038|D08734_70040|D08734_70066|D08734_70074|J06575_10557|J06575_10701|J06575_50735|J06575_50778|J06575_50892&revSciZip=null&revSciAge=null&revSciGender=null&division=Newspaper&SSTSCode=gci-az-phoenix.com/video/news_Video_prestream&videoId=768106922001&playerID=49625183001&playerKey=AQ~~,AAAABvZFMzE~,IXjx0MpOF0pugpuviAwD9l3_WMhvmNP7&domain=embed&dynamicStreaming=true" base="http://admin.brightcove.com" name="flashObj" width="486" height="412" seamlesstabbing="false" type="application/x-shockwave-flash" allowFullScreen="true" swLiveConnect="true" allowScriptAccess="always" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"></embed></object>
</div>
<a href="http://www.azcentral.com/video/#/K%2D9+bulletproof+vests/768106922001" target="PPK9_external">http://www.azcentral.com/video/#/K%2D9+bulletproof+vests/768106922001</a>

<h3>News Archive</h3>

<dl class="news">

<dt>01.30.2014 - PPK9 vests Cochise County Sheriff's K9 "Nobe"</dt>
<dd><a href="/docs/news/svherald_ppk9_2014_01_30.pdf" target="PPK9_external">Read article in "The Sierra Vista Herald"</a></dd>

<dt>01.24.2014 - PPK9 vests Yuma Police Dept. K9</dt>
<dd><a href="/docs/news/2014-01-24-Yuma-Vest-presentation.pdf" target="PPK9_external">Read press release</a></dd>

<dt>04.03.2009 - Group provides vests to police dogs</dt>
<dd><a href="http://www.zwire.com/site/news.cfm?newsid=20292197" target="PPK9_external">http://www.zwire.com/site/news.cfm?newsid=20292197</a></dd>

<dt>12.14.2006 - Names of DPS dogs now honor memory of fallen officers</dt>
<dd><a href="http://www.azstarnet.com/sn/relatedstories/160373.php" target="PPK9_external">http://www.azstarnet.com/sn/relatedstories/160373.php</a></dd>

<dt>09.22.2006 - Fans of Fallen Tucson Police K-9 Miko raise $10,000 for New Police K-9</dt>
<dd><a href="http://www.azstarnet.com/metro/147841" target="PPK9_external">http://www.azstarnet.com/metro/147841</a></dd>

<dt>09.21.2006 - Tucson Cops Get New K-9 Partner. News Story on KGUN 9</dt>
<dd><a href="http://www.kgun9.com/NewsArticle/tabid/1112/xmid/4862/Default.aspx" target="PPK9_external">http://www.kgun9.com/NewsArticle/tabid/1112/xmid/4862/Default.aspx</a></dd>

<dt>02.16.2006 - Protecting our Protectors</dt>
<dd><a href="http://www.kindnews.org/kids/KIND_Kid_hall_Valdez.asp" target="PPK9_external">http://www.kindnews.org/kids/KIND_Kid_hall_Valdez.asp</a></dd>

<dt>10.24.2005 - Humane Association Honors 13-Year-Old Boy for His Dedication to Police Dogs</dt>
<dd><a href="http://www.policemag.com/t_newspick.cfm?rank=75606" target="PPK9_external">http://www.policemag.com/t_newspick.cfm?rank=75606</a></dd>

<dt class="outdated">08.04.2005 - Arizona Central</dt>
<dd class="outdated">http://www.azcentral.com/news/articles/0816copdog16.html</dd>

<dt>05.18.2005 - American Humane: 2005 Kid Contest Winners</dt>
<dd><a href="http://www.americanhumane.org/site/PageServer?pagename=ev_public_bkaw_winners05" target="PPK9_external">http://www.americanhumane.org/site/PageServer?pagename=ev_public_bkaw_winners05</a></dd>

<dt class="outdated">08.27.2004 - Mesa City Manager's Update, August 27</dt>
<dd class="outdated">http://www.cityofmesa.org/citymgt/city_mgr_updates/8-27-04.asp</dd>
<dd class="outdated"><em>(look under "POLICE" heading)</em></dd>

<dt class="outdated">2004 - Kohl's "Kids Who Care"</dt>
<dd class="outdated">http://www.kohlscorporation.com/CommunityRelations/Community02A.htm</dd>

<dt class="outdated">2004 - Pets Corner's K-9 Info Page</dt>
<dd class="outdated">http://www.thepetscorner.com/home/pck9dogs.htm</dd>

<dt class="outdated">2004 - WSPA Teams With Vest-A-Dog</dt>
<dd class="outdated">http://www.wspa-americas.org/get_involved-police-dog2.htm</dd>

</dl>

<p><em>"protecting the dogs who faithfully protect us"</em></p>
<p>Please direct all media inquiries to <a href="mailto:reagen.kulseth@protectpolicek-9.com" title="Reagen.Kulseth [at] ProtectPoliceK-9.com">Reagen Kulseth</a>.</p>
<p>Report broken links to <a href="mailto:xonelabs@protectpolicek-9.com" title="XoneLabs [at] ProtectPoliceK-9.com">the webmaster</a>.</p>
</div>

<?php

include 'includes/footer.php' ;

?>