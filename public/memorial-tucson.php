<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>

<div class="maindiv">

<h1>Tucson Police Department Memorial Plaza</h1>

<p>In 2004, the Tucson Police Department was awarded a federal grant to redesign the lawn of their Headquarters Building with a memorial in honor of TPD's fallen officers and all law enforcement officers killed in the line of duty.  At the heart of the project is a bronze and tile art piece by local artist Judith Stewart, which honors the men and women who have chosen law enforcement as their profession.  To complete the memorial, Jennifer Jones, a Landscape Architecture student at the University of Arizona, has designed a practical and powerful tribute - <strong>The Tucson Police Department Memorial Plaza</strong>.</p>
<h3>We Need Your Help</h3>
<p>This outstanding and functional design has exceeded all expectations, as well as the limited funds of the federal grant. In response, the Tucson Police Foundation, Police Officers Association, and Tucson Fraternal Order of Police are conducting a fundraising campaign to facilitate the construction of the memorial.  We are reaching out to the community for donations in support of this beautiful tribute to the law enforcement officers who serve our community daily with diligence and committment. Please consider making a contribution to the <strong>Tucson Police Department Memorial Plaza</strong>.</p>
<h3>How To Make A Donation</h3>
<p>You can make a direct, secure online donation via the <a href="http://www.tucsonpolicefoundation.org/" target="PPK9_external">Tucson Police Foundation website</a>, or by visiting any Wells Fargo Bank branch to make a deposit in account number <em>6528395657</em>, the <em>Tucson Police Foundation New Memorial Fund</em>.</p>
<p>Additionally, <strong>members of the community are encouraged to make a lasting contribution through the purchase of a <a href="docs/TPDmemorial-inscriptions.pdf" target="PPK9_external">Flagstone Paver</a></strong>, which will be placed 


 permanently

 in the Memorial Plaza.  This is a joint fundraising project by the <acronym title="Tucson Police Officers Association">TPOA</acronym> and the <acronym title="Fraternal Order of Police, Lodge #1">FOP (Lodge #1)</acronym>.</p>
<p>For complete details about the "Inscriptions of Inspiration" Flagstone Paver project, please view/print the <a href="docs/TPDmemorial-inscriptions.pdf" target="PPK9_external">information brochure and order form</a>.</p>
<p>We are grateful for your continued support for the members of the Tucson Police Department. For more information, please see the <a href="http://www.ci.tucson.az.us/police/Hot_Topics/Memorial_Plaza/memorial_plaza.html" target="PPK9_external">official Tucson Police Memorial Plaza website</a>.</p>
</div>

<?php

include 'includes/footer.php' ;

?>
