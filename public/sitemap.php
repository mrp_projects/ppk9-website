<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>


<div class="maindiv">

<h1>Site Map</h1>

<ul>
<li><a href="http://www.protectpolicek-9.com">Protect Police K-9 Index</a>
	<ul>
	<li><a href="http://www.protectpolicek-9.com/news.php">Protect Police K-9 News Archive</a></li>
	<li><a href="http://www.protectpolicek-9.com/events.php">Upcoming Protect Police K-9 Events</a></li>
	<li><a href="http://www.protectpolicek-9.com/faq.php">PPK-9 F.A.Q.</a></li>
	<li><a href="http://www.protectpolicek-9.com/directors.php">Board of Directors</a></li>
	<li><!--<a href="http://www.protectpolicek-9.com/photos.php">-->PPK-9 Photo Gallery<!--</a>--></li>
	</ul></li>

<li><a href="http://www.protectpolicek-9.com/vested.php">Police Dogs Vested</a>
	<ul>
	<li><a href="http://www.protectpolicek-9.com/request.php">Request a Vest for your K-9 or Unit</li>
	<li><a href="http://www.protectpolicek-9.com/fallen.php">Fallen K9s</a></li>
	<li><a href="http://www.protectpolicek-9.com/memorial-tucson.php">Tucson Police Department Memorial Plaza</a></li>
	<li><a href="http://www.protectpolicek-9.com/memorial-arizona.php">Arizona Police K-9 Memorial</a></li>
	</ul></li>

<li><a href="http://www.protectpolicek-9.com/help.php">How you Can Help</a>
	<ul>
	<li><a href="http://www.protectpolicek-9.com/supporters.php">PPK9's Generous Sponsors</a></li>
	<li><a href="http://www.protectpolicek-9.com/merchandise.php">PPK9 themed gear</a> (buy shirts, hats, keyrings, etc. to support the cause)</li>
	<li><a href="http://www.protectpolicek-9.com/guestbook.php">PPK9 Guestbook</a> (leave a message of encouragement for the team)</li>
	</ul></li>

<li><a href="http://www.protectpolicek-9.com/links.php">Protect Police K-9 Useful Links</a>
	<ul>
	<li><a href="http://www.protectpolicek-9.com/contact.php">Contact Protect Police K-9</a></li>
	<li><a href="http://www.protectpolicek-9.com/request.php">K-9 Vest Request Form</a></li>
	<li><a href="http://www.protectpolicek-9.com/sitemap.php">Protect Police K-9 Sitemap</a></li>
	</ul></li>


</ul>

<p>Design and hosting management provided by <b>Michael Rog</b> at <b><a href="http://www.xonelabs.com">XoneLabs.com</a></b></p>

<p>This website, including all graphics, images, and content, belongs to <b><a href="http://www.protectpolicek-9.com">Arizona Protect Police K-9</a></b> and is protected under copyright law against reproduction or distribution of any kind.</p>

<p>
<a href="http://validator.w3.org/check?uri=referer">
<img style="border:0;width:88px;height:31px" src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0!" />
</a>
</p>
<p>
<a href="http://jigsaw.w3.org/css-validator/validator?uri=http://www.protectpolicek-9.com">
<img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" />
</a>
</p>

</div>

<?php

include 'includes/footer.php' ;

?>