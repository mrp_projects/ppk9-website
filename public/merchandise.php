<?php

include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;

?>


<div class="maindiv">

<h1>PPK9 <i>Fallen Canines</i> Keyrings</h1>

<div class="block">

<img src="images/products/keychain-Dax.gif" class="bod" alt="PPK9 Fallen Canines Keychain - K9 Dax" />

<p>

The story of K-9 Dax, an Arizona police dog wounded in the line of duty, inspired Michael Valdez to found <b>Protect Police K-9</b>.  Recently deceased, K-9 Dax is memorialized in this elegant keyring.

</p><p>

<b>$20</b> (includes shipping)<br />
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=Protectpolicek%2d9%40cox%2enet&item_name=Fallen%20Canines%20Keyring%20%2d%20K%2d9%20%22Dax%22&amount=20%2e00&no_shipping=0&no_note=1&currency_code=USD&lc=US&charset=UTF%2d8&charset=UTF%2d8"><img src="images/paypal/buynow.gif" alt="Buy Now"/></a>

</p>

</div>

<hr />

<div class="block">

<img src="images/products/keychain-Rico.gif" class="bod" alt="PPK9 Fallen Canines Keychain - K9 Dax" />

<p>

The second keyring in PPK9's <i>Fallen Canines</i> depicts another Arizona service dog, K-9 Rico, who died in the line of duty in 2004.

</p>
<p>

<b>$20</b> (includes shipping)<br />
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=Protectpolicek%2d9%40cox%2enet&item_name=Fallen%20Canines%20Keyring%20%2d%20K%2d9%20%22Rico%22&amount=20%2e00&no_shipping=0&no_note=1&currency_code=USD&lc=US&charset=UTF%2d8&charset=UTF%2d8"><img src="images/paypal/buynow.gif" alt="Buy Now"/></a>

</p>

</div>

</div>

<?php

include 'includes/footer.php' ;

?>