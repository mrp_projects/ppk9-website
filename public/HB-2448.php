<?php
include 'includes/header.php' ;
include 'includes/masthead.php' ;
include 'includes/navdiv.php' ;
?>

<div class="maindiv">

<h1>You can help protect our Police dogs.</h1>

<p>Protect Police K9 is presently working with Arizona legislators on House Bill 2448, which proposes stiffer laws regarding the protection of working police dogs and and provides state funding for K9 vests.  <a href="http://www.azleg.state.az.us/MembersPage.asp?Member_ID=31" target="PPK9_external">Ms. Linda Lopez (dist. 29)</a> is the primary sponsor of the bill.</p>
<p><b>We invite you to <a href="docs/HB-2448.pdf">view a (recently updated) working copy of HB-2448</a> and contact us with any inquiries. </b></p>

<hr />

<p>
You can also help support our efforts by contacting your elected representatives to encourage their support of HB-2448:
</p>

<ul>
<li><p><b>Step 1:  Identify your legislators...</b></p>
	
	<ul><li>
	<p><b>Arizona State Legislature</b></p>
	<p>The AZ Legislature website, <a href="http://www.azleg.state.az.us" target="PPK9_external">http://www.azleg.state.az.us</a>, provides a list of <a href="http://www.azleg.state.az.us/MemberRoster.asp?Body=H" target="PPK9_external"><b>Representatives</b></a> and <a href="http://www.azleg.state.az.us/MemberRoster.asp?Body=S" target="PPK9_external"><b>Senators</b></a> from each legislative district.</p>
	</li>

	<li>
	<p><b>Offices of Linda Lopez</b></p>
	<p>If you have trouble identifying your elected representatives, or if you wish to discuss your involvement in support of HB-2448, feel free to contact the <a href="http://www.azleg.state.az.us/MembersPage.asp?Member_ID=31" target="PPK9_external">Ms. Lopez's offices</a> for more information:</p>
	<p><b>Capitol Office:</b>  602.926.4089<br /><b>Toll Free:</b> 800.352.8404<br /><b>Fax:</b> 602.417.3029<br /><b>TDD:</b> 602.926.3241<br /><b>Tucson Office:</b> 520.398.6000</p>
	<p>Ms. Linda Lopez<br />Arizona House of Representatives<br />1700 W. Washington Street, Ste. H<br />Phoenix, Arizona 85007-2844</p>
	<p><b>email: </b>llopez (at) azleg.state.az.us</p>
	</li></ul>
</li>
<li><p><b>Step 2: Write to your legislators...</b></p>
	<p><ul>
	<li><p>Relax! Think about your letter as a letter to a friend who wants to hear your opinion on this very important issue.</p></li>
	<li><p>The most effective letter is a personal letter, not a form letter. The key to our success is the show of concern from constituents like you, who care about our service animals.  Personal correspondence will carry that message; a form letter or postcard only defeats the purpose.</p></li>
	<li><p>Clearly identify yourself in the first paragraph.</p></li>
	<li><p>Write your letter on personal or business stationary if you have it.  If not, plain paper is fine.  Always include your return address in the body of the letter - envelopes get lost.</p></li>
	<li><p>Address your legislator respectfully, using their last name.  "The Honorable ____" is the most proper title of address, though "Mr./Mrs. ____" is acceptable.</p></li>
	<li><p>Keep your letter brief and to the point. Letters should be no more than one page. A paragraph or two is fine. You can always attach additional information or, even better, send follow-up letters.</p></li>
	<li><p>Refer to House Bill 2448 by name and number.</p></li>
	<li><p>Write in your own words. Even if you have a sample letter to follow, change it around so it sounds individual.</p></li>
	<li><p>Use a polite and respectful tone in your letter. Never be rude or threatening!  Ask for a reply. After you receive their reply, consider responding to reiterate your opinion, to thank them and to let them know that you will be following the issue and how they approach it.</p></li>
	<li><p>Conclude your letter by urging the legislator to take action in support of your position, and thank him or her for taking the time to consider your views.</li>
	<li><p>Write and mail your letter as soon as possible - legislation can move quickly, and legislators need to hear from you before they vote!</p></li>
	</ul></p>
</li>
</ul>

<p>
Your help is deeply appreciated.  Only through your support are we able to protect our Police K-9s.
</p>

</div>

<?php

include 'includes/footer.php' ;

?>